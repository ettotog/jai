package io.gitlab.ettotog.jai.sample;

import lombok.AllArgsConstructor;

import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
public class InMultipleFiles<I, O> implements LearningSample<I, O> {

    private final List<Path> files;

    private Function<Path, I> inputMapper;
    private Function<Path, O> outputMapper;

    private final int inputWidth;
    private final int outputWidth;

    @Override
    public int getSize() {
        return files.size();
    }

    @Override
    public int getInputWidth() {
        return inputWidth;
    }

    @Override
    public int getOutputWidth() {
        return outputWidth;
    }

    @Override
    public I getInput(int index) {
        return inputMapper.apply(files.get(index));
    }

    @Override
    public O getOutput(int index) {
        return outputMapper.apply(files.get(index));
    }

    @Override
    public String toString() {
        return "InMultipleFiles{" +
                "files=" + files +
                '}';
    }
}
