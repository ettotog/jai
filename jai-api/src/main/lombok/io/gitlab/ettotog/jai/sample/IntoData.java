package io.gitlab.ettotog.jai.sample;

import java.util.List;

public interface IntoData<C, D> {

    D toData(C clazz);

    default D toData(C... classes) {
        return toData(List.of(classes));
    }

    D toData(List<C> classes);
}
