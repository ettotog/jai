package io.gitlab.ettotog.jai.sample;

import java.util.List;
import java.util.Map;

public interface IntoClass<C, D> {

    C toClass(D data);

    List<C> toClasses(D data);

    Map<C, Double> toProbabilities(D data);
}
