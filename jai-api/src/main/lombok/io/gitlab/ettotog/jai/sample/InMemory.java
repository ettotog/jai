package io.gitlab.ettotog.jai.sample;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class InMemory<I,O> implements LearningSample<I, O> {

    private List<I> inputs = new ArrayList<>();
    private List<O> outputs = new ArrayList<>();

    private final int inputWidth;
    private final int outputWidth;

    public InMemory(int inputWidth, int outputWidth) {
        this.inputWidth = inputWidth;
        this.outputWidth = outputWidth;
    }

    public InMemory<I,O> addSample(I input, O output) {
        inputs.add(input);
        outputs.add(output);
        return this;
    }

    public InMemory<I,O> build() {
        InMemory<I,O> copy = new InMemory<>(inputWidth, outputWidth);
        copy.outputs = List.copyOf(outputs);
        copy.inputs = List.copyOf(inputs);
        return copy;
    }

    @Override
    public int getSize() {
        return inputs.size();
    }

    @Override
    public int getInputWidth() {
        return inputWidth;
    }

    @Override
    public int getOutputWidth() {
        return outputWidth;
    }

    @Override
    public I getInput(int index) {
        return inputs.get(index);
    }

    @Override
    public O getOutput(int index) {
        return outputs.get(index);
    }


}
