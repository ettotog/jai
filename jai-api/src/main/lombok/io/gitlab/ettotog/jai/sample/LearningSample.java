package io.gitlab.ettotog.jai.sample;

import io.gitlab.ettotog.jai.math.Pair;
import io.gitlab.ettotog.jai.math.vector.Vector;

import java.util.Iterator;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface LearningSample<I, O> extends Iterable<Pair<I, O>> {

    int getSize();

    int getInputWidth();

    int getOutputWidth();

    I getInput(int index);

    O getOutput(int index);

    default Stream<Pair<I, O>> stream() {
        return IntStream.range(0, getSize())
                .mapToObj(it -> new Pair<>(getInput(it), getOutput(it)));
    }

    default Iterator<Pair<I, O>> iterator() {
        return stream().iterator();
    }

    default void forEach(Consumer<? super Pair<I, O>> action) {
        stream().forEach(action);
    }


    static LearningSample<Vector<Double>, Vector<Double>> noopWithBias(double trueValue, double falseValue) {
        InMemory<Vector<Double>, Vector<Double>> inMemory = new InMemory<>(2, 1);
        inMemory.addSample(Vector.of(trueValue, falseValue), Vector.of(falseValue));
        inMemory.addSample(Vector.of(trueValue, trueValue), Vector.of(trueValue));
        return inMemory.build();
    }

    static LearningSample<Vector<Double>, Vector<Double>> notWithBias() {
        InMemory<Vector<Double>, Vector<Double>> inMemory = new InMemory<>(2, 1);
        inMemory.addSample(Vector.of(1d, 0d), Vector.of(1d));
        inMemory.addSample(Vector.of(1d, 1d), Vector.of(0d));
        return inMemory.build();
    }

    static LearningSample<Vector<Double>, Vector<Double>> andWithBias() {
        InMemory<Vector<Double>, Vector<Double>> inMemory = new InMemory<>(3, 1);
        inMemory.addSample(Vector.of(1d, 0d, 0d), Vector.of(0d));
        inMemory.addSample(Vector.of(1d, 0d, 1d), Vector.of(0d));
        inMemory.addSample(Vector.of(1d, 1d, 0d), Vector.of(0d));
        inMemory.addSample(Vector.of(1d, 1d, 1d), Vector.of(1d));
        return inMemory.build();
    }

    static LearningSample<Vector<Double>, Vector<Double>> orWithBias() {
        InMemory<Vector<Double>, Vector<Double>> inMemory = new InMemory<>(3, 1);
        inMemory.addSample(Vector.of(1d, 0d, 0d), Vector.of(0d));
        inMemory.addSample(Vector.of(1d, 0d, 1d), Vector.of(1d));
        inMemory.addSample(Vector.of(1d, 1d, 0d), Vector.of(1d));
        inMemory.addSample(Vector.of(1d, 1d, 1d), Vector.of(1d));
        return inMemory.build();
    }

    static LearningSample<Vector<Double>, Vector<Double>> xorWithBias() {
        InMemory<Vector<Double>, Vector<Double>> inMemory = new InMemory<>(3, 1);
        inMemory.addSample(Vector.of(1d, 0d, 0d), Vector.of(0d));
        inMemory.addSample(Vector.of(1d, 0d, 1d), Vector.of(1d));
        inMemory.addSample(Vector.of(1d, 1d, 0d), Vector.of(1d));
        inMemory.addSample(Vector.of(1d, 1d, 1d), Vector.of(0d));
        return inMemory.build();
    }

    static LearningSample<Vector<Double>, Vector<Double>> binaryToDecimal() {
        InMemory<Vector<Double>, Vector<Double>> inMemory = new InMemory<>(5, 10);
        VectorClassMapper<Integer> mapper = VectorClassMapper.from(IntStream.range(0, 10).boxed().collect(Collectors.toList()));
        inMemory.addSample(Vector.of(0d, 0d, 0d, 0d), mapper.toData(0));
        inMemory.addSample(Vector.of(0d, 0d, 0d, 1d), mapper.toData(1));
        inMemory.addSample(Vector.of(0d, 0d, 1d, 0d), mapper.toData(2));
        inMemory.addSample(Vector.of(0d, 0d, 1d, 1d), mapper.toData(3));
        inMemory.addSample(Vector.of(0d, 1d, 0d, 0d), mapper.toData(4));
        inMemory.addSample(Vector.of(0d, 1d, 0d, 1d), mapper.toData(5));
        inMemory.addSample(Vector.of(0d, 1d, 1d, 0d), mapper.toData(6));
        inMemory.addSample(Vector.of(0d, 1d, 1d, 1d), mapper.toData(7));
        inMemory.addSample(Vector.of(1d, 0d, 0d, 0d), mapper.toData(8));
        inMemory.addSample(Vector.of(1d, 0d, 0d, 1d), mapper.toData(9));
        return inMemory.build();
    }
}
