package io.gitlab.ettotog.jai.sample;

import io.gitlab.ettotog.jai.math.Pair;
import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.AllArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.gitlab.ettotog.jai.math.vector.Vector.sum;

@AllArgsConstructor
public class VectorClassMapper<C> implements IntoClass<C, Vector<Double>>, IntoData<C, Vector<Double>> {

    private final Map<C, Vector<Double>> classToData;
    private final List<C> indexToClass;

    @SafeVarargs
    public static <E> VectorClassMapper<E> from(E... classes) {
        return from(List.of(classes));
    }

    public static <E> VectorClassMapper<E> from(List<E> classes) {
        Map<E, Vector<Double>> classMap = new HashMap<>(classes.size());
        for (int i = 0; i < classes.size(); i += 1) {
            classMap.put(classes.get(i), Vector.indexed(classes.size(), Set.of(i), 1d, 0d));
        }
        return new VectorClassMapper<>(classMap, List.copyOf(classes));
    }

    public Vector<Double> toData(C clazz) {
        if (!classToData.containsKey(clazz)) throw new IllegalArgumentException();
        return classToData.get(clazz);
    }

    @SafeVarargs
    public final Vector<Double> toData(C... classes) {
        return toData(List.of(classes));
    }

    public Vector<Double> toData(List<C> classes) {
        return classes.stream()
                .map(classToData::get)
                .reduce(Vector::sum)
                .orElseThrow(IllegalArgumentException::new);
    }

    public C toClass(Vector<Double> data) {
        int max = Vector.max(data);
        return indexToClass.get(max);
    }

    public List<C> toClasses(Vector<Double> data) {
        double avg = Vector.avg(data);
        return IntStream.range(0, data.size())
                .mapToObj(index -> new Pair<>(index, data.get(index)))
                .filter(p -> p.getLast() >= avg)
                .sorted((a, b) -> -a.getLast().compareTo(b.getLast()))
                .map(Pair::getFirst)
                .map(indexToClass::get)
                .collect(Collectors.toList());
    }

    public Map<C, Double> toProbabilities(Vector<Double> data) {
        Double min = data.values()
                .min(Double::compare)
                .map(it -> it > 0 ? 0 : it)
                .orElse(0d);
        Vector<Double> normalized = sum(data, Vector.repeat(data.size(), min));
        Double total = normalized.values().reduce(Double::sum).orElse(0d);
        return IntStream.range(0, indexToClass.size())
                .boxed()
                .collect(Collectors.toMap(indexToClass::get, it -> normalized.get(it) / total));
    }
}
