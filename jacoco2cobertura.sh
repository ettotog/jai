#!/bin/sh
for path in $(find . -iname "jacoco.xml"); do
  module=$(echo $path | cut -d/ -f2)
  python /opt/cover2cover.py $path $CI_PROJECT_DIR/$module/src/main/lombok/ > ${module}-cobertura.xml
done
