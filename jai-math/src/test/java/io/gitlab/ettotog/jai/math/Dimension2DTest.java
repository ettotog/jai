package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.vector.Vector;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class Dimension2DTest {

    @Test
    public void test() {
        DimensionalSize dimensionalSize = DimensionalSize.of(3, 5);
        Dimension2D dimension2D = Dimension2D.of(dimensionalSize);

        List<Vector<Integer>> expected = dimension2D.stream().map(Point2D::asVector).collect(Collectors.toList());
        List<Vector<Integer>> result = dimension2D.asDimensionalSize().indexes().collect(Collectors.toList());

        assertThat(result).isEqualTo(expected);

    }

    @Test
    public void conversion() {

        DimensionalSize dimensionalSize = DimensionalSize.of(5, 3);
        assertThat(dimensionalSize).isEqualTo(Dimension2D.of(3, 5).asDimensionalSize());

        Dimension2D dimension2D = Dimension2D.of(3, 5);
        assertThat(dimension2D).isEqualTo(Dimension2D.of(DimensionalSize.of(5, 3)));
    }
}
