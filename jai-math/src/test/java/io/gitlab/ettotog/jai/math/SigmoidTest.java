package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.functions.Sigmoid;
import lombok.val;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SigmoidTest {
    Sigmoid sigmoid = new Sigmoid();
    @Test
    public void test() {
        assertThat(sigmoid.apply(0d)).isEqualTo(0.5d);
        assertThat(sigmoid.apply(4d)).isCloseTo(1d, Offset.offset(0.1));
        assertThat(sigmoid.apply(-4d)).isCloseTo(0d, Offset.offset(0.1));
    }

    @Test
    public void testDerivative() {
        val derivative = sigmoid.derivative();
        assertThat(derivative.apply(sigmoid.apply(4d))).isCloseTo(0d, Offset.offset(0.1));
        assertThat(derivative.apply(sigmoid.apply(-4d))).isCloseTo(0d, Offset.offset(0.1));
    }

}
