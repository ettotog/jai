package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.vector.Vector;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class Dimension3DTest {

    @Test
    public void test() {
        Dimension3D dimension3D = Dimension3D.of(5, 5 ,5);

        List<Vector<Integer>> expected = dimension3D.stream().map(Point3D::asVector).collect(Collectors.toList());
        List<Vector<Integer>> result = dimension3D.asDimensionalSize().indexes().collect(Collectors.toList());

        assertThat(result).isEqualTo(expected);

    }
}
