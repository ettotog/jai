package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.functions.TanH;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

public class TanHTest {

    TanH tanH = new TanH();

    @Test
    void test() {
        assertThat(tanH.apply(0d)).isZero();
        assertThat(tanH.apply(4d)).isCloseTo(1, Offset.offset(0.1));
        assertThat(tanH.apply(-4d)).isCloseTo(-1, Offset.offset(0.1));
    }

    @Test
    void testDerivative() {
        Function<Double,Double> derivative = tanH.derivative();
        assertThat(derivative.apply(tanH.apply(0d))).isEqualTo(1d);
        assertThat(derivative.apply(tanH.apply(4d))).isCloseTo(0, Offset.offset(0.1));
        assertThat(derivative.apply(tanH.apply(-4d))).isCloseTo(0, Offset.offset(0.1));
    }
}
