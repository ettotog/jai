package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.matrix.Matrix;
import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.val;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class MatrixTest {


    @Test
    public void convolutionTest() {
        Matrix<String> matrix = Matrix.from(Dimension2D.of(3, 3), Vector.from(
                "a", "b", "c",
                "d",  "e", "f",
                "g", "h", "i"
        ));
        Matrix<Double> kernel = Matrix.from(Dimension2D.of(3, 3), Vector.of(
                1d, 2d, 3d,
                4d, 5d, 6d,
                7d, 8d, 9d
        ));
        val result = kernel.convolution(matrix).limit(9).collect(Collectors.toList());
        val expected = List.of(
                Pair.of(
                        Point2D.of(0, 0),
                        Vector.from(Stream.of(
                                Pair.of("a", 1d), Pair.of("a", 2d), Pair.of("b", 3d),
                                Pair.of("a", 4d), Pair.of("a", 5d), Pair.of("b", 6d),
                                Pair.of("d", 7d), Pair.of("d", 8d), Pair.of("e", 9d)
                        ))
                ),
                Pair.of(
                        Point2D.of(0, 1),
                        Vector.from(Stream.of(
                                Pair.of("a", 1d), Pair.of("b", 2d), Pair.of("c", 3d),
                                Pair.of("a", 4d), Pair.of("b", 5d), Pair.of("c", 6d),
                                Pair.of("d", 7d), Pair.of("e", 8d), Pair.of("f", 9d)
                        ))
                ),
                Pair.of(
                        Point2D.of(0, 2),
                        Vector.from(Stream.of(
                                Pair.of("b", 1d), Pair.of("c", 2d), Pair.of("c", 3d),
                                Pair.of("b", 4d), Pair.of("c", 5d), Pair.of("c", 6d),
                                Pair.of("e", 7d), Pair.of("f", 8d), Pair.of("f", 9d)
                        ))
                ),
                Pair.of(
                        Point2D.of(1, 0),
                        Vector.from(Stream.of(
                                Pair.of("a", 1d), Pair.of("a", 2d), Pair.of("b", 3d),
                                Pair.of("d", 4d), Pair.of("d", 5d), Pair.of("e", 6d),
                                Pair.of("g", 7d), Pair.of("g", 8d), Pair.of("h", 9d)
                        ))
                ),
                Pair.of(
                        Point2D.of(1, 1),
                        Vector.from(Stream.of(
                                Pair.of("a", 1d), Pair.of("b", 2d), Pair.of("c", 3d),
                                Pair.of("d", 4d), Pair.of("e", 5d), Pair.of("f", 6d),
                                Pair.of("g", 7d), Pair.of("h", 8d), Pair.of("i", 9d)
                        ))
                ),
                Pair.of(
                        Point2D.of(1, 2),
                        Vector.from(Stream.of(
                                Pair.of("b", 1d), Pair.of("c", 2d), Pair.of("c", 3d),
                                Pair.of("e", 4d), Pair.of("f", 5d), Pair.of("f", 6d),
                                Pair.of("h", 7d), Pair.of("i", 8d), Pair.of("i", 9d)
                        ))
                ),
                Pair.of(
                        Point2D.of(2, 0),
                        Vector.from(Stream.of(
                                Pair.of("d", 1d), Pair.of("d", 2d), Pair.of("e", 3d),
                                Pair.of("g", 4d), Pair.of("g", 5d), Pair.of("h", 6d),
                                Pair.of("g", 7d), Pair.of("g", 8d), Pair.of("h", 9d)
                        ))
                ),
                Pair.of(
                        Point2D.of(2, 1),
                        Vector.from(Stream.of(
                                Pair.of("d", 1d), Pair.of("e", 2d), Pair.of("f", 3d),
                                Pair.of("g", 4d), Pair.of("h", 5d), Pair.of("i", 6d),
                                Pair.of("g", 7d), Pair.of("h", 8d), Pair.of("i", 9d)
                        ))
                ),
                Pair.of(
                        Point2D.of(2, 2),
                        Vector.from(Stream.of(
                                Pair.of("e", 1d), Pair.of("f", 2d), Pair.of("f", 3d),
                                Pair.of("h", 4d), Pair.of("i", 5d), Pair.of("i", 6d),
                                Pair.of("h", 7d), Pair.of("i", 8d), Pair.of("i", 9d)
                        ))
                )
        );
        assertThat(result).isEqualTo(expected);
    }

}
