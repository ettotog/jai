package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.vector.Vector;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DimensionalSizeTest {


    @Test
    public void indexes() {
        DimensionalSize dimensionalSize = DimensionalSize.of(3, 4, 2);
        List<Vector<Integer>> expected = List.of(
                Vector.of(0, 0, 0),
                Vector.of(0, 0, 1),
                Vector.of(0, 1, 0),
                Vector.of(0, 1, 1),
                Vector.of(0, 2, 0),
                Vector.of(0, 2, 1),
                Vector.of(0, 3, 0),
                Vector.of(0, 3, 1),
                Vector.of(1, 0, 0),
                Vector.of(1, 0, 1),
                Vector.of(1, 1, 0),
                Vector.of(1, 1, 1),
                Vector.of(1, 2, 0),
                Vector.of(1, 2, 1),
                Vector.of(1, 3, 0),
                Vector.of(1, 3, 1),
                Vector.of(2, 0, 0),
                Vector.of(2, 0, 1),
                Vector.of(2, 1, 0),
                Vector.of(2, 1, 1),
                Vector.of(2, 2, 0),
                Vector.of(2, 2, 1),
                Vector.of(2, 3, 0),
                Vector.of(2, 3, 1)
        );
        List<Vector<Integer>> result = new ArrayList<>(expected.size());
        dimensionalSize.indexes().forEach(result::add);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void asIndex() {
        DimensionalSize size = DimensionalSize.of(3, 4, 2);
        size.range().forEach(index -> assertThat(index).isEqualTo(size.asIndex(size.asIndex(index))));
        size.indexes().forEach(index -> assertThat(index).isEqualTo(size.asIndex(size.asIndex(index))));
    }
}
