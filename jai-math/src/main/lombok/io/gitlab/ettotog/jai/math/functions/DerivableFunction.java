package io.gitlab.ettotog.jai.math.functions;

import java.io.Serializable;
import java.util.function.Function;

public interface DerivableFunction extends Function<Double, Double>, Serializable {
    Function<Double, Double> derivative();
}
