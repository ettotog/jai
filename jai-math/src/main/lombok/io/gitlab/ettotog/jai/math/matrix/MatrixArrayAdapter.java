package io.gitlab.ettotog.jai.math.matrix;

import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import io.gitlab.ettotog.jai.math.array.Array;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MatrixArrayAdapter<E> implements Matrix<E> {

    private final Array<E> source;

    @Override
    public Dimension2D getSize() {
        return Dimension2D.of(source.size());
    }

    @Override
    public E get(Point2D<Integer> point) {
        return source.get(point.asVector());
    }

    public static <E> MatrixArrayAdapter<E> of(Array<E> source) {
        if (source.size().count() != 2) throw new IllegalArgumentException();
        return new MatrixArrayAdapter<>(source);
    }
}
