package io.gitlab.ettotog.jai.math.matrix;

import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import lombok.AllArgsConstructor;

import java.util.stream.Stream;

@AllArgsConstructor
public class SafeMatrix<E> implements Matrix<E> {

    private final Matrix<E> source;

    @Override
    public Dimension2D getSize() {
        return source.getSize();
    }

    @Override
    public E get(Point2D<Integer> point) {
        return source.get(getSize().near(point));
    }

    @Override
    public Stream<Entry<E>> entries() {
        return source.entries();
    }
}
