package io.gitlab.ettotog.jai.math.functions;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.function.Function;

@NoArgsConstructor
@EqualsAndHashCode
public class TanH implements DerivableFunction {

    @Override
    public Function<Double, Double> derivative() {
        //return  it -> 1 - (Math.tanh(it)*Math.tanh(it));
        return it -> 1 - (it * it);
    }

    @Override
    public Double apply(Double aDouble) {
        return Math.tanh(aDouble);
    }

    @Override
    public String toString() {
        return "f(x) = tanh(x)";
    }
}
