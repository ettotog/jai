package io.gitlab.ettotog.jai.math.vector;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.stream.Stream;

@AllArgsConstructor
public class FlyweightDoubleVector implements Vector<Double> {

    private final double[] origin;
    private final int offset;
    private final int length;

    @Override
    public int size() {
        return this.length;
    }

    @Override
    public Double get(int index) {
        return origin[offset + index];
    }

    @Override
    public Stream<Double> values() {
        return Arrays.stream(origin, offset, offset + length).boxed();

    }
}
