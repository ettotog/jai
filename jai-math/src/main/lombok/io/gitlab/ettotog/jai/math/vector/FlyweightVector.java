package io.gitlab.ettotog.jai.math.vector;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.stream.Stream;

@AllArgsConstructor
public class FlyweightVector<E> implements Vector<E> {

    private final E[] origin;
    private final int offset;
    private final int length;

    @Override
    public int size() {
        return this.length;
    }

    @Override
    public E get(int index) {
        return origin[offset + index];
    }

    @Override
    public Stream<E> values() {
        return Arrays.stream(origin, offset, offset + length);

    }
}
