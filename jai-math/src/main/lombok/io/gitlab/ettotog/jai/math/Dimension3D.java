package io.gitlab.ettotog.jai.math;

import lombok.AllArgsConstructor;

import java.util.stream.IntStream;
import java.util.stream.Stream;

@AllArgsConstructor
public class Dimension3D {

    private final int width;
    private final int height;
    private final int length;

    public static Dimension3D of(int width, int height, int length) {
        return new Dimension3D(width, height, length);
    }

    public static Dimension3D of(int side) {
        return new Dimension3D(side, side, side);
    }

    public DimensionalSize asDimensionalSize() {
        return DimensionalSize.of(height, width, length);
    }

    public Stream<Point3D<Integer>> stream() {
        return IntStream.range(0, height)
                .boxed()
                .flatMap(y -> IntStream.range(0, width).mapToObj(x -> Point2D.of(y, x)))
                .flatMap(p2d -> IntStream.range(0, length).mapToObj(p2d::addZ));
    }

    public int asIndex(Point3D<Integer> position) {
        return position.getY() * width + position.getX() * width + position.getZ();
    }
}
