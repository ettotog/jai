package io.gitlab.ettotog.jai.math.vector;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.stream.Stream;

@AllArgsConstructor
public class DoubleArrayAdapter extends BaseVector<Double> {

    private final double[] array;

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public Double get(int index) {
        return array[index];
    }

    @Override
    public Stream<Double> values() {
        return Arrays.stream(array).boxed();
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }

}
