package io.gitlab.ettotog.jai.math.vector;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.stream.Stream;

@AllArgsConstructor
public class IntArrayAdapter extends BaseVector<Integer> {

    private final int[] array;

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public Integer get(int index) {
        return array[index];
    }

    @Override
    public Stream<Integer> values() {
        return Arrays.stream(array).boxed();
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }

}
