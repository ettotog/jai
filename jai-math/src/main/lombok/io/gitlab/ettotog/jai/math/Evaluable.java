package io.gitlab.ettotog.jai.math;

import lombok.AllArgsConstructor;

@FunctionalInterface
public interface Evaluable<E> {

    E evaluate();

    static <E> Evaluable<E> constant(E value) {
        return new Const<>(value);
    }

    @AllArgsConstructor
    class Const<E> implements Evaluable<E> {
        private final E value;

        @Override
        public E evaluate() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        static final Const<Double> ZERO = new Const<>(0d);
        static final Const<Double> ONE = new Const<>(1d);
    }

    static Evaluable<Double> zero() {
        return Evaluable.Const.ZERO;
    }
    static Evaluable<Double> one() {
        return Evaluable.Const.ONE;
    }
}
