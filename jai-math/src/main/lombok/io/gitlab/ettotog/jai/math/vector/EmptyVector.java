package io.gitlab.ettotog.jai.math.vector;

import lombok.NoArgsConstructor;

import java.util.stream.Stream;

@NoArgsConstructor
public class EmptyVector<E> implements Vector<E> {

    private static final EmptyVector<?> INSTANCE = new EmptyVector<>();

    @Override
    public int size() {
        return 0;
    }

    @Override
    public E get(int index) {
        throw new IllegalArgumentException();
    }

    @Override
    public Stream<E> values() {
        return Stream.empty();
    }

    @SuppressWarnings("unchecked")
    public static <E> EmptyVector<E> getInstance() {
        return (EmptyVector<E>) INSTANCE;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Vector)) return false;
        return ((Vector<?>) obj).size() == 0;
    }

    @Override
    public String toString() {
        return "[]";
    }
}
