package io.gitlab.ettotog.jai.math.functions;

import io.gitlab.ettotog.jai.math.Evaluable;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
@Deprecated
public class Softmax implements DerivableFunction {

    private List<Evaluable<Double>> layer;

    public double expSum() {
        return layer.stream()
                .map(Evaluable::evaluate)
                .map(Math::exp)
                .reduce(Double::sum)
                .orElse(0d);
    }

    @Override
    public Function<Double, Double> derivative() {
        return it -> it / expSum();
    }

    @Override
    public Double apply(Double value) {
        return Math.exp(value) / expSum();
    }
}
