package io.gitlab.ettotog.jai.math;

public interface Translatable<T extends Translatable, E extends Number> {

    T translated(Point2D<E> point);

}
