package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public abstract class Point2D<E extends Number> implements Translatable<Point2D<E>, E> {
    private final E y;
    private final E x;

    public static Point2D<Integer> of(int y, int x) {
        return new IntPoint2D(y, x);
    }

    public static Point2D<Double> of(double y, double x) {
        return new DoublePoint2D(y, x);
    }

    public abstract Point2D<E> translated(Point2D<E> other);

    public abstract double length();

    public <T extends Translatable<T, E>> T translate(Translatable<T, E> translatable) {
        return translatable.translated(this);
    }

    public Vector<E> asVector() {
        return Vector.from(y, x);
    }

    public Point3D<E> addZ(E z) {
        return Point3D.of(y, x, z);
    }

    static class DoublePoint2D extends Point2D<Double> {

        public DoublePoint2D(Double y, Double x) {
            super(y, x);
        }

        @Override
        public Point2D<Double> translated(Point2D<Double> other) {
            return Point2D.of(getY() + other.getY(), getX() + other.getX());
        }

        @Override
        public double length() {
            return Math.sqrt(getX() * getX() + getY() * getY());
        }
    }

    static class IntPoint2D extends Point2D<Integer> {

        public IntPoint2D(Integer y, Integer x) {
            super(y, x);
        }

        @Override
        public Point2D<Integer> translated(Point2D<Integer> other) {
            return Point2D.of(getY() + other.getY(), getX() + other.getX());
        }

        @Override
        public double length() {
            return Math.sqrt(getX() * getX() + getY() * getY());
        }
    }

}
