package io.gitlab.ettotog.jai.math;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.function.BiFunction;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
@ToString
public class Pair<A, B> {

    private final A first;
    private final B last;

    public static <A, B> Pair<A, B> of(A a, B b) {
        return new Pair<>(a, b);
    }

    public <R> R reduce(BiFunction<A, B, R> fn) {
        return fn.apply(first, last);
    }

}
