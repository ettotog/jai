package io.gitlab.ettotog.jai.math.vector;

import io.gitlab.ettotog.jai.math.Entry;
import io.gitlab.ettotog.jai.math.Pair;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface Vector<E> extends Serializable {

    static int max(Vector<Double> vector) {
        int biggerIndex = 0;
        Double bigger = vector.get(0);
        for (int i = 1; i < vector.size(); i += 1) {
            Double current = vector.get(i);
            if (current > bigger) {
                biggerIndex = i;
                bigger = current;
            }
        }
        return biggerIndex;
    }

    static Double avg(Vector<Double> vector) {
        return vector.values()
                .reduce(0d, Double::sum) / vector.size();
    }

    static <T> Collector<T, List<T>, Vector<T>> collector() {
        return new ListVectorCollector<>();
    }

    default <T> Vector<T> apply(Function<E, T> fn) {
        return from(values().map(fn));
    }

    default <T> Stream<T> map(Function<E, T> fn) {
        return values().map(fn);
    }


    default IntStream find(Predicate<E> predicate) {
        return IntStream.range(0, size())
                .filter(it -> predicate.test(get(it)));
    }

    static Vector<Double> sum(Vector<Double> a, Vector<Double> b) {
        return from(zip(a, b).map(p -> p.getFirst() + p.getLast()).toArray(Double[]::new));
    }

    static Vector<Double> diff(Vector<Double> a, Vector<Double> b) {
        return new ArrayAdapter<>(zip(a, b).map(p -> p.getFirst() - p.getLast()).toArray(Double[]::new));
    }

    static double meanSquaredError(Vector<Double> a, Vector<Double> b) {
        return zip(a, b).map(p -> Math.pow(p.getFirst() - p.getLast(), 2)).reduce(0d, Double::sum) / a.size();
    }

    static double accuracy(Vector<Double> a, Vector<Double> b) {
        return zip(a, b).map(p -> p.getFirst() / p.getLast()).reduce(0d, Double::sum) / a.size();
    }

    static <A, B> Stream<Pair<A, B>> zip(Vector<A> a, Vector<B> b) {
        return IntStream.range(0, a.size()).mapToObj(index -> new Pair<>(a.get(index), b.get(index)));
    }

    static <E> Vector<E> empty() {
        return EmptyVector.getInstance();
    }

    static Vector<Double> zeros(int size) {
        return of(IntStream.range(0, size).mapToDouble(it -> 0d).toArray());
    }

    static <E> Vector<E> repeat(int size, E value) {
        return IntStream.range(0, size).mapToObj(it -> value).collect(collector());
    }

    static <E> Vector<E> from(Stream<E> stream) {
        return stream.collect(collector());
    }

    static <E> Vector<E> from(E[] array, int offset, int length) {
        return new FlyweightVector<>(array, offset, length);
    }

    static Vector<Double> from(double[] array, int offset, int length) {
        return new FlyweightDoubleVector(array, offset, length);
    }

    static Vector<Double> indexed(int size, Set<Integer> set, Double setValue, Double unsetValue) {
        return IntStream.range(0, size)
                .mapToObj(it -> set.contains(it) ? setValue : unsetValue)
                .collect(collector());
    }

    int size();

    E get(int index);

    Stream<E> values();

    default Vector<E> extended(E value) {
        return from(Stream.concat(values(), Stream.of(value)));
    }

    default Stream<Integer> keys() {
        return IntStream.range(0, size()).boxed();
    }

    default Stream<Entry<Integer, E>> entries() {
        return keys().map(it -> Entry.of(it, get(it)));
    }

    static Vector<Double> of(double... doubles) {
        return new DoubleArrayAdapter(doubles);
    }

    static Vector<Integer> of(int... ints) {
        return new IntArrayAdapter(ints);
    }

    @SafeVarargs
    static <E> Vector<E> from(E... values) {
        return new ArrayAdapter<>(values);
    }

    static Vector<Double> random(int size) {
        return of(IntStream.range(0, size).mapToDouble(it -> (Math.random() * 2) - 1).toArray());
    }

    static <E> Vector<E> from(List<E> list) {
        return new ListAdapter<>(list);
    }


}
