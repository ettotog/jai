package io.gitlab.ettotog.jai.math.functions;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.function.Function;

@EqualsAndHashCode
public class ReLU implements DerivableFunction {

    private static final double min = 0;

    @Override
    public Function<Double, Double> derivative() {
        return it -> it > min ? 1d : 0d;
    }

    @Override
    public Double apply(Double aDouble) {
        return Math.max(min, aDouble);
    }

    @Override
    public String toString() {
        return String.format("f(x) = if x > %f { x } else { 0 }", min);
    }
}
