package io.gitlab.ettotog.jai.math.vector;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.stream.Stream;

@AllArgsConstructor
public class ArrayAdapter<E> extends BaseVector<E> {
    private final E[] array;


    @Override
    public int size() {
        return array.length;
    }

    @Override
    public E get(int index) {
        return array[index];
    }

    @Override
    public Stream<E> values() {
        return Stream.of(array);
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}
