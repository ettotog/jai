package io.gitlab.ettotog.jai.math.matrix;

import io.gitlab.ettotog.jai.math.Point2D;

public interface MutableMatrix<E> extends Matrix<E> {

    void set(Point2D<Integer> point, E value);

    void set(Entry<E> entry);
}
