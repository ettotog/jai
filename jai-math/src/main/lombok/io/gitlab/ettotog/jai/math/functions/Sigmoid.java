package io.gitlab.ettotog.jai.math.functions;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.function.Function;

@NoArgsConstructor
@EqualsAndHashCode
public class Sigmoid implements DerivableFunction {

    @Override
    public Double apply(Double aDouble) {
        return 1 / (1 + Math.pow(Math.E, -aDouble));
    }

    public Double applyDerivative(Double aDouble) {
        return aDouble * (1 - aDouble);
    }

    public Function<Double, Double> derivative() {
        return this::applyDerivative;
    }

    public String toString() {
        return "f(x) = 1 / ( 1 + E ^ -x)";
    }

}
