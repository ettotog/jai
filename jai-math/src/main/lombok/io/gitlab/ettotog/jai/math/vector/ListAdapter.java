package io.gitlab.ettotog.jai.math.vector;

import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Stream;

@AllArgsConstructor
public class ListAdapter<E> extends BaseVector<E> {
    private final List<E> list;

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public E get(int index) {
        return list.get(index);
    }

    @Override
    public Stream<E> values() {
        return list.stream();
    }

    @Override
    public String toString() {
        return list.toString();
    }
}
