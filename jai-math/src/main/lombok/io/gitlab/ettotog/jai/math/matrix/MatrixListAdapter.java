package io.gitlab.ettotog.jai.math.matrix;

import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class MatrixListAdapter<E> implements MutableMatrix<E> {

    private final List<E> content;
    private final Dimension2D size;

    @Override
    public Dimension2D getSize() {
        return size;
    }

    @Override
    public E get(Point2D<Integer> point) {
        return content.get(size.asIndex(point));
    }

    @Override
    public void set(Point2D<Integer> point, E value) {
        content.set(size.asIndex(point), value);
    }

    @Override
    public void set(Entry<E> entry) {
        content.set(size.asIndex(entry.getKey()), entry.getValue());
    }


}
