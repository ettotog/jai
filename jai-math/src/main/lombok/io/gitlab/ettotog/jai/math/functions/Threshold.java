package io.gitlab.ettotog.jai.math.functions;

import lombok.EqualsAndHashCode;

import java.util.function.Function;

@EqualsAndHashCode
public class Threshold implements DerivableFunction {

    private final double value = 0d;
    private final double upValue = 1d;
    private final double downValue = 0d;

    @Override
    public Function<Double, Double> derivative() {
        return it -> 1d;
    }

    @Override
    public Double apply(Double aDouble) {
        return aDouble >= value ? upValue : downValue;
    }

    @Override
    public String toString() {
        return String.format("f(x) = if x >= %f { %f }  else { %f }", value, upValue, downValue);
    }
}
