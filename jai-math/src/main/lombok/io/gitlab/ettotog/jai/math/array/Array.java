package io.gitlab.ettotog.jai.math.array;

import io.gitlab.ettotog.jai.math.DimensionalSize;
import io.gitlab.ettotog.jai.math.Entry;
import io.gitlab.ettotog.jai.math.vector.Vector;

import java.util.stream.Stream;

public interface Array<E> {

    static <T> Array<T> of(DimensionalSize size, Vector<T> output) {
        return new ArrayVectorAdapter<>(size, output);
    }

    DimensionalSize size();

    E get(Vector<Integer> vector);


    default Array<E> sliced(Slicer slicer) {
        return slicer.sliced(this);
    }

    default Stream<Entry<Vector<Integer>, E>> slice(Slicer slicer) {
        return slicer.slice(this);
    }

    Stream<E> values();

    Stream<Vector<Integer>> keys();

    Stream<Entry<Vector<Integer>, E>> entries();


}
