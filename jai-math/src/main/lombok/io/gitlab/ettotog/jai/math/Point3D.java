package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class Point3D<E extends Number> {
    private final E y;
    private final E x;
    private final E z;

    public static <E extends Number> Point3D<E> of(E y, E x, E z) {
        return new Point3D<>(y, x, z);
    }

    public Vector<E> asVector() {
        return Vector.from(y, x, z);
    }
}
