package io.gitlab.ettotog.jai.math.vector;

public abstract class BaseVector<E> implements Vector<E> {
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Vector)) return false;
        Vector<?> that = (Vector<?>) other;
        return Vector.zip(this, that).allMatch(p -> p.getFirst().equals(p.getLast()));
    }
}
