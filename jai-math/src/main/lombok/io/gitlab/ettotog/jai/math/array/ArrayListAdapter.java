package io.gitlab.ettotog.jai.math.array;

import io.gitlab.ettotog.jai.math.DimensionalSize;
import io.gitlab.ettotog.jai.math.Entry;
import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Stream;

@AllArgsConstructor
public class ArrayListAdapter<E> implements Array<E> {

    private final DimensionalSize size;
    private final List<E> content;

    @Override
    public DimensionalSize size() {
        return size;
    }

    @Override
    public E get(Vector<Integer> vector) {
        return content.get(size.asIndex(vector));
    }

    @Override
    public Stream<E> values() {
        return content.stream();
    }

    @Override
    public Stream<Vector<Integer>> keys() {
        return size.indexes();
    }

    @Override
    public Stream<Entry<Vector<Integer>, E>> entries() {
        return size.range().map(index -> Entry.of(size.asIndex(index), content.get(index)));
    }
}
