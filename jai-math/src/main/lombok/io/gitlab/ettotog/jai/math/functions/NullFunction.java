package io.gitlab.ettotog.jai.math.functions;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.function.Function;

@NoArgsConstructor
@EqualsAndHashCode
public class NullFunction implements DerivableFunction {

    @Override
    public Function<Double, Double> derivative() {
        return this;
    }

    @Override
    public Double apply(Double aDouble) {
        return aDouble;
    }
}

