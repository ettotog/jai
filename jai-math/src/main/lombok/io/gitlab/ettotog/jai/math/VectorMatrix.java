package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.matrix.Matrix;
import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class VectorMatrix<E> implements Matrix<E> {

    private final Vector<E> data;
    private final Dimension2D size;

    @Override
    public Dimension2D getSize() {
        return size;
    }

    @Override
    public E get(Point2D<Integer> point) {
        return data.get(size.asIndex(point));
    }
}
