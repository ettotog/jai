package io.gitlab.ettotog.jai.math.array;

import io.gitlab.ettotog.jai.math.DimensionalSize;
import io.gitlab.ettotog.jai.math.Entry;
import io.gitlab.ettotog.jai.math.vector.Vector;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Slicer {

    <E> Stream<Entry<Vector<Integer>, E>> slice(Array<E> array);

    <E> Array<E> sliced(Array<E> array);

    static Slicer last(int index) {
        return new Slicer() {
            @Override
            public <E> Stream<Entry<Vector<Integer>, E>> slice(Array<E> array) {
                return array.size()
                        .reduced()
                        .indexes()
                        .map(it -> it.extended(index))
                        .map(index -> Entry.of(index, array.get(index)));
            }

            @Override
            public <E> Array<E> sliced(Array<E> array) {
                DimensionalSize reduced = array.size().reduced();
                List<E> collect = reduced
                        .indexes()
                        .map(it -> it.extended(index))
                        .map(array::get)
                        .collect(Collectors.toList());
                return new ArrayListAdapter<>(reduced, collect);
            }
        };


    }
}
