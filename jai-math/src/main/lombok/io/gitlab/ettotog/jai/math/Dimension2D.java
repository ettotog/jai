package io.gitlab.ettotog.jai.math;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@lombok.Builder(toBuilder = true)
public class Dimension2D implements Serializable {

    private final int width;
    private final int height;

    public int size() {
        return height * width;
    }

    public Dimension2D(int side) {
        this(side, side);
    }

    public Stream<Point2D<Integer>> stream() {
        return IntStream.range(0, height)
                .boxed()
                .flatMap(y -> IntStream.range(0, width).mapToObj(x -> Point2D.of(y, x)));
    }

    public int asIndex(Point2D<Integer> position) {
        return position.getY() * width + position.getX();
    }

    public Point2D<Integer> near(Point2D<Integer> point) {
        return Point2D.of(
                Math.min(Math.max(point.getY(), 0), height - 1),
                Math.min(Math.max(point.getX(), 0), width - 1)
        );
    }

    public Point2D<Integer> circular(Point2D<Integer> point) {
        return Point2D.of(
                point.getY() % height,
                point.getX() % width
        );
    }

    public Point2D<Double> relative(Point2D<Integer> point) {
        return Point2D.of(
                (double) point.getY() / height,
                (double) point.getX() / width
        );
    }

    public static Dimension2D of(int width, int height) {
        return new Dimension2D(width, height);
    }

    public static Dimension2D of(int side) {
        return new Dimension2D(side, side);
    }

    public static Dimension2D of(DimensionalSize size) {
        return new Dimension2D(size.size(1), size.size(0));
    }

    public DimensionalSize asDimensionalSize() {
        return DimensionalSize.of(height, width);
    }

    public Dimension2D scale(double scale) {
        return Dimension2D.of((int) Math.round(width * scale), (int) Math.round(height * scale));
    }
}
