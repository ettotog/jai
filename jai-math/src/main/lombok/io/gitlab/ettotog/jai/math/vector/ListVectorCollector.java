package io.gitlab.ettotog.jai.math.vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListVectorCollector<T> implements Collector<T, List<T>, Vector<T>> {
    @Override
    public Supplier<List<T>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<T>, T> accumulator() {
        return List::add;
    }

    @Override
    public BinaryOperator<List<T>> combiner() {
        return (a, b) -> Stream.concat(a.stream(), b.stream()).collect(Collectors.toList());
    }

    @Override
    public Function<List<T>, Vector<T>> finisher() {
        return Vector::from;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of();
    }
}
