package io.gitlab.ettotog.jai.math;

import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface DimensionalSize extends Serializable {

    /**
     * @return How many dimensions
     */
    int count();

    /**
     * @return Product of all dimensions
     */
    int size();

    int size(int index);

    DimensionalSize extended(int size);

    DimensionalSize reduced();

    Stream<Vector<Integer>> indexes();

    Stream<Integer> range();

    Vector<Integer> asIndex(int index);

    int asIndex(Vector<Integer> index);

    static DimensionalSize of(int... sizes) {
        return new Impl(Vector.of(sizes));
    }

    static DimensionalSize of(Vector<Integer> sizes) {
        return new Impl(sizes);
    }


    @AllArgsConstructor
    @EqualsAndHashCode
    class Impl implements DimensionalSize {

        private final Vector<Integer> dimensionsSizes;

        @Override
        public int count() {
            return dimensionsSizes.size();
        }

        @Override
        public int size() {
            return dimensionsSizes.values().reduce(1, (a, b) -> a * b);
        }

        @Override
        public Vector<Integer> asIndex(int index) {
            int[] counters = new int[count()];
            for (int i = count() - 1; i >= 0; i -= 1) {
                int size = dimensionsSizes.get(i);
                counters[i] = index % size;
                index /= size;
                if (index == 0) break;
            }
            return Vector.of(counters);
        }

        @Override
        public int asIndex(Vector<Integer> index) {
            return index
                    .entries()
                    .map(entry -> dimensionsSizes.values().skip(entry.getKey() + 1).reduce(1, (a, b) -> a * b) * entry.getValue())
                    .reduce(0, Integer::sum);
        }

        @Override
        public Stream<Integer> range() {
            return IntStream.range(0, size()).boxed();
        }

        @Override
        public Stream<Vector<Integer>> indexes() {
            return range().map(this::asIndex);
        }

        @Override
        public DimensionalSize extended(int size) {
            return DimensionalSize.of(Vector.from(Stream.concat(
                    dimensionsSizes.values(),
                    Stream.of(size)
            )));
        }

        @Override
        public DimensionalSize reduced() {
            return DimensionalSize.of(Vector.from(
                    dimensionsSizes.values().limit(count() - 1)
            ));
        }

        @Override
        public String toString() {
            return String.valueOf(dimensionsSizes);
        }

        @Override
        public int size(int index) {
            return dimensionsSizes.get(index);
        }
    }


}
