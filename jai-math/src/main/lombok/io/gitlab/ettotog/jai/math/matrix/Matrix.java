package io.gitlab.ettotog.jai.math.matrix;

import io.gitlab.ettotog.jai.math.*;
import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.*;

import java.util.function.Function;
import java.util.stream.Stream;

public interface Matrix<E> {

    @AllArgsConstructor
    @Getter
    @EqualsAndHashCode
    @ToString
    class Entry<E> implements Translatable<Entry<E>, Integer> {
        private final Point2D<Integer> key;

        private final E value;

        public Integer getY() {
            return key.getY();
        }

        public Integer getX() {
            return key.getX();
        }

        @Override
        public Entry<E> translated(Point2D<Integer> point) {
            val newPosition = Point2D.of(key.getY() + point.getY(), key.getX() + point.getX());
            return new Entry<>(newPosition, value);
        }

        public static <V> Entry<V> of(Point2D<Integer> key, V value) {
            return new Entry<>(key, value);
        }
    }

    Dimension2D getSize();

    E get(Point2D<Integer> point);

    default Stream<Entry<E>> entries() {
        return getSize()
                .stream()
                .map(point -> new Entry<>(point, get(point)));
    }

    static <E> Matrix<E> from(Dimension2D size, Vector<E> data) {
        if (size.size() > data.size()) throw new IllegalArgumentException();
        return new VectorMatrix<>(data, size);
    }

    default <T> Stream<Pair<Point2D<Integer>, Vector<Pair<T, E>>>> convolution(Matrix<T> matrix) {
        val safeMatrix = new SafeMatrix<>(matrix);
        val xOffset = (getSize().getWidth() - 1) / 2;
        val yOffset = (getSize().getHeight() - 1) / 2;
        val offset = Point2D.of(-yOffset, -xOffset);
        val kernel = Vector.from(entries().map(it -> it.translated(offset)));
        return matrix
                .getSize()
                .stream()
                .map(point -> Pair.of(point, Vector.from(kernel.values().map(point::translate).map(it -> Pair.of(safeMatrix.get(it.getKey()), it.getValue())))));

    }

    default <T> Vector<T> asVector(Function<E, T> mapper) {
        return entries()
                .map(Entry::getValue)
                .map(mapper)
                .collect(Vector.collector());
    }
}
