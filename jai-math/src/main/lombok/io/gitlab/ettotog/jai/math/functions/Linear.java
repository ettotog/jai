package io.gitlab.ettotog.jai.math.functions;

import lombok.EqualsAndHashCode;

import java.util.function.Function;

@EqualsAndHashCode
public class Linear implements DerivableFunction {

    private final double multiplier = 1;

    @Override
    public Function<Double, Double> derivative() {
        return it -> multiplier;
    }

    @Override
    public Double apply(Double aDouble) {
        return aDouble * multiplier;
    }

    @Override
    public String toString() {
        return String.format("f(x) = %f * x + 0", multiplier);
    }
}
