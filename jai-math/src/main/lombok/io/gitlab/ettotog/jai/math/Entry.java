package io.gitlab.ettotog.jai.math;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

public interface Entry<K, V> {

    K getKey();

    V getValue();


    @Getter
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    class Impl<K, V> implements Entry<K, V> {
        private final K key;
        private final V value;
    }

    static <A, B> Entry<A, B> of(A key, B value) {
        return new Impl<>(key, value);
    }
}
