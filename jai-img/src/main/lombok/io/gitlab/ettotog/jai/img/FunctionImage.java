package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import lombok.AllArgsConstructor;

import java.util.function.Function;

@AllArgsConstructor
public class FunctionImage implements Image {

    private Dimension2D size;
    private Function<Point2D<Integer>, Color> colorFunction;

    @Override
    public Dimension2D getSize() {
        return size;
    }

    @Override
    public Color get(Point2D<Integer> point) {
        return colorFunction.apply(point);
    }
}
