package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.img.math.MatrixImage;
import io.gitlab.ettotog.jai.img.math.VectorImage;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import io.gitlab.ettotog.jai.math.matrix.Matrix;
import io.gitlab.ettotog.jai.math.vector.Vector;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Image extends Matrix<Color> {

    default Stream<Entry<Color>> entries() {
        return getSize()
                .stream()
                .map(point -> new Entry<>(point, get(point)));
    }


    default Image scaled(Dimension2D size) {
        return new ScaledImage(size, this);
    }

    default Image scaled(double scale) {
        return new ScaledImage(getSize().scale(scale), this);
    }

    static <E> Image from(Dimension2D size, Function<Point2D<Integer>, Color> mapper) {
        return new FunctionImage(size, mapper);
    }

    static <E> Image from(Dimension2D size, Vector<E> vector, Function<E, Color> mapper) {
        return new VectorImage<E>(size, vector, mapper);
    }

    static <E> Image from(Matrix<E> matrix, Function<E, Color> mapper) {
        return new MatrixImage<>(matrix, mapper);
    }


    default Stream<Matrix.Entry<Color>> linearFilter(Matrix<Double> kernel) {
        //noinspection OptionalGetWithoutIsPresent
        return kernel
                .convolution(this)
                .map(it -> Matrix.Entry.of(it.getFirst(), it.getLast().map(p -> p.reduce(Color::scale)).reduce(Color::sum).get()));
    }

    @FunctionalInterface
    interface Writer {
        void write(Image image, OutputStream outputStream) throws IOException;

        default void write(Image image, File file) throws IOException {
            write(image, new FileOutputStream(file));
        }

        default void write(Image image, Path path) throws IOException {
            write(image, path.toFile());
        }

    }

    @FunctionalInterface
    interface Reader {
        Image read(InputStream inputStream) throws IOException;

        default Image read(File file) throws IOException {
            return read(new FileInputStream(file));
        }

        default Image read(URL url) throws IOException {
            return read(url.openStream());
        }

        default Image read(Path path) throws IOException {
            return read(path.toFile());
        }
    }

    interface IO extends Reader, Writer {


    }

}
