package io.gitlab.ettotog.jai.img.color;


import lombok.*;

import java.util.stream.Stream;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
@lombok.Builder(toBuilder = true)
public class HSV implements Color {

    private final double hue;
    private final double saturation;
    private final double value;

    public static HSV of(double h, double s, double v) {
        return new HSV(h, s, v);
    }

    public static final HSV BLACK = HSV.of(0, 0, 0);
    public static final HSV WHITE = HSV.of(0, 0, 1);
    public static final HSV RED = HSV.of(0, 1, 1);
    public static final HSV GREEN = HSV.of(120, 1, 1);
    public static final HSV BLUE = HSV.of(240, 1, 1);
    public static final HSV YELLOW = HSV.of(60, 1, 1);
    public static final HSV CYAN = HSV.of(180, 1, 1);
    public static final HSV MAGENTA = HSV.of(300, 1, 1);

    @Override
    public RGB asRGB() {
        double C = value * saturation;
        double X = C * (1 - Math.abs((hue / 60) % 2 - 1));
        double m = value - C;
        double r = 0, g = 0, b = 0;
        if (hue < 60) {
            r = C;
            g = X;
            b = 0;
        } else if (hue < 120) {
            r = X;
            g = C;
            b = 0;
        } else if (hue < 180) {
            r = 0;
            g = C;
            b = X;
        } else if (hue < 240) {
            r = 0;
            g = X;
            b = C;
        } else if (hue < 300) {
            r = X;
            g = 0;
            b = C;
        } else if (hue < 360) {
            r = C;
            g = 0;
            b = X;
        }
        return new RGB((int) Math.round((r + m) * 255.), (int) Math.round((g + m) * 255), (int) Math.round((b + m) * 255));
    }

    public static HSV from(RGB rgb) {
        double R = rgb.getRed() / 255.;
        double G = rgb.getGreen() / 255.;
        double B = rgb.getBlue() / 255.;
        double cMax = Stream.of(R, G, B).reduce(Double::max).orElse(0d);
        double cMin = Stream.of(R, G, B).reduce(Double::min).orElse(0d);
        double delta = cMax - cMin;
        double hl = delta == 0 ? 0
                : cMax == R ? ((G - B) / delta) % 6
                : cMax == G ? ((B - R) / delta) + 2
                : cMax == B ? ((R - G) / delta) + 4 : 0d;
        double h = 60 * hl;
        if (h < 0) h = 360. + h;
        double s = cMax == 0d ? 0d : delta / cMax;
        return new HSV(h, s, cMax);
    }

    @Override
    public HSV asHSV() {
        return this;
    }

    @Override
    public Color added(Color color) {
        HSV hsv = color.asHSV();
        return HSV.of(hue + hsv.getHue(), saturation + hsv.getSaturation(), value + hsv.getValue());
    }

    @Override
    public Color mixed(Color color) {
        HSV hsv = color.asHSV();
        val hue = (getHue() * getSaturation() + hsv.getHue() * hsv.getSaturation()) / (getSaturation() + hsv.getSaturation());
        val saturation = (getSaturation() + hsv.getSaturation()) / 2;
        val value = (getValue() + hsv.getValue()) / 2;
        return HSV.of(hue, saturation, value);
    }

    @Override
    public Color scaled(double scale) {
        return HSV.of(hue, saturation * scale, value * scale);
    }
}
