package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.matrix.Matrix;
import io.gitlab.ettotog.jai.math.Point2D;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.awt.image.BufferedImage;
import java.util.function.Function;

@AllArgsConstructor
@Getter
public class BufferedImageFactory implements ImageFactory {

    private int imageType;

    @Override
    public MutableImage create(Dimension2D size) {
        return new BufferedImageAdapter(new BufferedImage(size.getWidth(), size.getHeight(), getImageType()));
    }

    @Override
    public MutableImage create(Dimension2D size, Function<Point2D<Integer>, Color> fn) {
        MutableImage mutableImage = create(size);
        size.stream()
                .map(point -> Matrix.Entry.of(point, fn.apply(point)))
                .forEach(mutableImage::set);
        return mutableImage;
    }

    @Override
    public int getImageType() {
        return imageType;
    }

    public static BufferedImageFactory rgb() {
        return new BufferedImageFactory(BufferedImage.TYPE_INT_RGB);
    }
    
}
