package io.gitlab.ettotog.jai.img;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;

public class BufferedImageReader implements Image.Reader {
    @Override
    public Image read(InputStream inputStream) throws IOException {
        return new BufferedImageAdapter(ImageIO.read(inputStream));
    }
}
