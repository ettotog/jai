package io.gitlab.ettotog.jai.img;

import lombok.AllArgsConstructor;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;

@AllArgsConstructor
public class BufferedImageRenderer implements ImageRenderer {

    private final ImageFactory imageFactory;


    @Override
    public RenderedImage render(Image image) {
        if (image instanceof ImageRenderer) {
            return (RenderedImage) image;
        }
        BufferedImage bufferedImage = new BufferedImage(image.getSize().getWidth(), image.getSize().getHeight(), imageFactory.getImageType());
        BufferedImageAdapter adapter = new BufferedImageAdapter(bufferedImage);
        image.entries().forEach(adapter::set);
        return adapter;
    }
}
