package io.gitlab.ettotog.jai.img;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public enum IO implements Image.IO {

    RGB_PNG("PNG", new BufferedImageRenderer(new BufferedImageFactory(BufferedImage.TYPE_INT_RGB)));

    private final String type;
    private final ImageRenderer imageRenderer;

    IO(String type, ImageRenderer imageRenderer) {
        this.type = type;
        this.imageRenderer = imageRenderer;
    }

    @Override
    public void write(Image image, OutputStream outputStream) throws IOException {
        ImageIO.write(imageRenderer.render(image), type, outputStream);
    }

    @Override
    public Image read(InputStream inputStream) throws IOException {
        return new BufferedImageAdapter(ImageIO.read(inputStream));
    }
}
