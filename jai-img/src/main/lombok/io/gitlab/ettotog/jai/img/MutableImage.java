package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.math.matrix.MutableMatrix;

public interface MutableImage extends Image, MutableMatrix<Color> {

}
