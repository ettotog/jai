package io.gitlab.ettotog.jai.img.math;

import io.gitlab.ettotog.jai.img.Image;
import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import io.gitlab.ettotog.jai.math.matrix.Matrix;
import lombok.AllArgsConstructor;

import java.util.function.Function;


@AllArgsConstructor
public class MatrixImage<E> implements Image {

    private final Matrix<E> source;
    private final Function<E, Color> mapper;

    @Override
    public Dimension2D getSize() {
        return source.getSize();
    }

    @Override
    public Color get(Point2D<Integer> point) {
        return mapper.apply(source.get(point));
    }
}
