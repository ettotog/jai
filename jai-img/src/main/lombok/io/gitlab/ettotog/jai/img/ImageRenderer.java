package io.gitlab.ettotog.jai.img;

import java.awt.image.RenderedImage;

public interface ImageRenderer {

    RenderedImage render(Image image);
}
