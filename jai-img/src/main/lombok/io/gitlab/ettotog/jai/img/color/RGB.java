package io.gitlab.ettotog.jai.img.color;

import lombok.*;

import java.util.stream.Stream;

@AllArgsConstructor
@lombok.Builder(toBuilder = true)
@Getter
@EqualsAndHashCode
@ToString
public class RGB implements Color {
    static final int RED_MASK = 0x00FF0000;
    static final int RED_INDEX = 16;
    static final int GREEN_MASK = 0x0000FF00;
    static final int GREEN_INDEX = 8;

    static final int BLUE_MASK = 0x000000FF;
    static final int BLUE_INDEX = 0;

    private final double red;
    private final double green;
    private final double blue;

    public static final RGB BLACK = RGB.of(0, 0, 0);
    public static final RGB WHITE = RGB.of(255, 255, 255);
    public static final RGB RED = RGB.of(255, 0, 0);
    public static final RGB GREEN = RGB.of(0, 255, 0);
    public static final RGB BLUE = RGB.of(0, 0, 255);
    public static final RGB YELLOW = RGB.of(255, 255, 0);
    public static final RGB CYAN = RGB.of(0, 255, 255);
    public static final RGB MAGENTA = RGB.of(255, 0, 255);


    public static RGB of(int value) {
        return new RGB(
                value, value, value
        );
    }

    public static RGB of(int r, int g, int b) {
        return new RGB(
                r, g, b
        );
    }

    public static RGB of(double r, double g, double b) {
        return new RGB(
                r, g, b
        );
    }


    public static RGB from(int raw) {
        return new RGB(
                (raw & RED_MASK) >> RED_INDEX,
                (raw & GREEN_MASK) >> GREEN_INDEX,
                (raw & BLUE_MASK) >> BLUE_INDEX
        );
    }

    public static RGB from(double raw) {
        int value = (int) (raw * 255);
        return new RGB(
                value,
                value,
                value
        );
    }

    public static RGB from(double r, double g, double b) {
        return new RGB(
                (int) (r * 255),
                (int) (g * 255),
                (int) (b * 255)
        );
    }

    public int toRaw() {
        return (Math.min(Math.round((float) red), 255) << RED_INDEX)
                + (Math.min(Math.round((float) blue), 255) << BLUE_INDEX)
                + (Math.min(Math.round((float) green), 255) << GREEN_INDEX);
    }

    @Override
    public RGB asRGB() {
        return this;
    }

    @Override
    public HSV asHSV() {
        return HSV.from(this);
    }

    public Double avg() {
        return Stream.of(red, green, blue).reduce(0d, Double::sum) / 3.;
    }

    @Override
    public Color added(Color color) {
        RGB rgb = color.asRGB();
        return RGB.of(red + rgb.getRed(), green + rgb.getGreen(), blue + rgb.getBlue());
    }

    public Color mixed(Color color) {
        RGB rgb = color.asRGB();
        return RGB.of((red + rgb.getRed()) / 2, (green + rgb.getGreen()) / 2, (blue + rgb.getBlue()) / 2);
    }

    @Override
    public Color scaled(double value) {
        return RGB.of(red * value, green * value, blue * value);
    }
}
