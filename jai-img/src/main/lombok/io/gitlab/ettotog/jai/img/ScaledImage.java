package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ScaledImage implements Image {

    private final Dimension2D size;
    private final Image origin;

    @Override
    public Dimension2D getSize() {
        return size;
    }

    @Override
    public Color get(Point2D<Integer> point) {
        Dimension2D originalSize = origin.getSize();
        Dimension2D currentSize = size;
        int x = point.getX() * originalSize.getWidth() / currentSize.getWidth();
        int y = point.getY() * originalSize.getHeight() / currentSize.getHeight();
        return origin.get(Point2D.of(y, x));
    }

    @Override
    public Image scaled(Dimension2D size) {
        if (size.equals(origin.getSize())) return origin;
        return new ScaledImage(size, origin);
    }
}
