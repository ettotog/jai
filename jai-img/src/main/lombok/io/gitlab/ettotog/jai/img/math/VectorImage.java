package io.gitlab.ettotog.jai.img.math;

import io.gitlab.ettotog.jai.img.Image;
import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.AllArgsConstructor;

import java.util.function.Function;

@AllArgsConstructor
public class VectorImage<E> implements Image {

    private final Dimension2D size;
    private final Vector<E> data;

    private final Function<E, Color> mapper;

    @Override
    public Dimension2D getSize() {
        return size;
    }

    @Override
    public Color get(Point2D<Integer> point) {
        return mapper.apply(data.get(size.asIndex(point)));
    }
}
