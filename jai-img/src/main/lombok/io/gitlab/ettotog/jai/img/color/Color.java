package io.gitlab.ettotog.jai.img.color;

public interface Color {

    RGB asRGB();

    HSV asHSV();

    Color added(Color color);

    Color mixed(Color color);

    Color scaled(double value);

    static Color sum(Color a, Color b) {
        return a.added(b);
    }

    static Color blend(Color a, Color b) {
        return a.mixed(b);
    }

    static Color scale(Color color, double value) {
        return color.scaled(value);
    }
}
