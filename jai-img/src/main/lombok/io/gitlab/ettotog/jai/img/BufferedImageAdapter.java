package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.img.color.RGB;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;
import lombok.AllArgsConstructor;

import java.awt.*;
import java.awt.image.*;
import java.util.Vector;

@AllArgsConstructor
public class BufferedImageAdapter implements MutableImage, RenderedImage {
    private final BufferedImage bufferedImage;

    @Override
    public Dimension2D getSize() {
        return Dimension2D.of(bufferedImage.getWidth(), bufferedImage.getHeight());
    }

    @Override
    public Color get(Point2D<Integer> point) {
        int rawRgb = bufferedImage.getRGB(point.getX(), point.getY());
        return RGB.from(rawRgb);
    }

    @Override
    public void set(Entry<Color> pixel) {
        bufferedImage.setRGB(pixel.getX(), pixel.getY(), pixel.getValue().asRGB().toRaw());
    }

    @Override
    public void set(Point2D<Integer> position, Color color) {
        set(Entry.of(position, color));
    }

    @Override
    public Vector<RenderedImage> getSources() {
        return bufferedImage.getSources();
    }

    @Override
    public Object getProperty(String name) {
        return bufferedImage.getProperty(name);
    }

    @Override
    public String[] getPropertyNames() {
        return bufferedImage.getPropertyNames();
    }

    @Override
    public ColorModel getColorModel() {
        return bufferedImage.getColorModel();
    }

    @Override
    public SampleModel getSampleModel() {
        return bufferedImage.getSampleModel();
    }

    @Override
    public int getWidth() {
        return bufferedImage.getWidth();
    }

    @Override
    public int getHeight() {
        return bufferedImage.getHeight();
    }

    @Override
    public int getMinX() {
        return bufferedImage.getMinX();
    }

    @Override
    public int getMinY() {
        return bufferedImage.getMinY();
    }

    @Override
    public int getNumXTiles() {
        return bufferedImage.getNumXTiles();
    }

    @Override
    public int getNumYTiles() {
        return bufferedImage.getNumYTiles();
    }

    @Override
    public int getMinTileX() {
        return bufferedImage.getMinTileX();
    }

    @Override
    public int getMinTileY() {
        return bufferedImage.getMinTileY();
    }

    @Override
    public int getTileWidth() {
        return bufferedImage.getTileWidth();
    }

    @Override
    public int getTileHeight() {
        return bufferedImage.getTileHeight();
    }

    @Override
    public int getTileGridXOffset() {
        return bufferedImage.getTileGridXOffset();
    }

    @Override
    public int getTileGridYOffset() {
        return bufferedImage.getTileGridYOffset();
    }

    @Override
    public Raster getTile(int tileX, int tileY) {
        return bufferedImage.getTile(tileX, tileY);
    }

    @Override
    public Raster getData() {
        return bufferedImage.getData();
    }

    @Override
    public Raster getData(Rectangle rect) {
        return bufferedImage.getData(rect);
    }

    @Override
    public WritableRaster copyData(WritableRaster raster) {
        return bufferedImage.copyData(raster);
    }
}
