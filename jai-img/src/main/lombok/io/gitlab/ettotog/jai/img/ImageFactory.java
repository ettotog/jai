package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.Point2D;

import java.awt.image.BufferedImage;
import java.util.function.Function;

public interface ImageFactory {

    MutableImage create(Dimension2D size);

    MutableImage create(Dimension2D size, Function<Point2D<Integer>, Color> fn);

    /**
     * @see BufferedImage#getType()
     */
    int getImageType();
}
