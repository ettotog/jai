package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.img.color.RGB;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.matrix.Matrix;
import io.gitlab.ettotog.jai.math.Pair;
import io.gitlab.ettotog.jai.math.vector.Vector;
import lombok.val;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class LaplaceTest {

    @Test
    public void laplaceOnHomogeneous() throws IOException {
        Color randomColor = RGB.from(Math.random(), Math.random(), Math.random());
        BufferedImageFactory imageFactory = BufferedImageFactory.rgb();
        BufferedImageRenderer imageRenderer = new BufferedImageRenderer(imageFactory);
        Dimension2D size = Dimension2D.of(16, 16);
        val inputImage = imageFactory.create(size, point -> randomColor);
        val outputImage = imageFactory.create(size);
        Matrix<Double> kernel = Matrix.from(Dimension2D.of(3), Vector.of(
                1d, 1d, 1d,
                1d, -8d, 1d,
                1d, 1d, 1d
        ));
        inputImage.linearFilter(kernel).forEach(outputImage::set);
        outputImage.entries()
                .map(Matrix.Entry::getValue)
                .forEach(color -> assertThat(color).isEqualTo(RGB.BLACK));

    }

    @Test
    public void laplaceOnHeterogeneous() throws IOException {
        BufferedImageFactory imageFactory = BufferedImageFactory.rgb();
        BufferedImageRenderer imageRenderer = new BufferedImageRenderer(imageFactory);
        Dimension2D size = Dimension2D.of(16, 16);
        val inputImage = imageFactory.create(size, point -> RGB.from(Math.random(), Math.random(), Math.random()));
        val outputImage = imageFactory.create(size);
        Matrix<Double> kernel = Matrix.from(Dimension2D.of(3), Vector.of(
                1d, 1d, 1d,
                1d, -8d, 1d,
                1d, 1d, 1d
        ));
        inputImage.linearFilter(kernel).forEach(outputImage::set);
        size.stream()
                .map(point -> Pair.of(inputImage.get(point), outputImage.get(point)))
                .forEach(p -> assertThat(p.getFirst()).isNotEqualTo(p.getLast()));

    }
}
