package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.Color;
import io.gitlab.ettotog.jai.img.color.HSV;
import io.gitlab.ettotog.jai.img.color.RGB;
import io.gitlab.ettotog.jai.math.Pair;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class ColorTest {

    @RepeatedTest(value = 100, name = RepeatedTest.LONG_DISPLAY_NAME)
    public void testHsvRgbConversion() {
        Random random = new Random();
        RGB rgb = RGB.from(random.nextDouble(), random.nextDouble(), random.nextDouble());
        HSV hsv = rgb.asHSV();
        assertThat(hsv.asRGB()).isEqualTo(rgb);
    }

    public static Stream<Pair<Color, Color>> baseColors() {
        return Stream.of(
                Pair.of(HSV.BLACK, RGB.BLACK),
                Pair.of(HSV.WHITE, RGB.WHITE),
                Pair.of(HSV.RED, RGB.RED),
                Pair.of(HSV.GREEN, RGB.GREEN),
                Pair.of(HSV.BLUE, RGB.BLUE),
                Pair.of(HSV.YELLOW, RGB.YELLOW),
                Pair.of(HSV.CYAN, RGB.CYAN),
                Pair.of(HSV.MAGENTA, RGB.MAGENTA)
        );
    }

    @ParameterizedTest
    @MethodSource("baseColors")
    public void testBaseColorsConversion(Pair<Color, Color> colorPair) {
        assertThat(colorPair.getFirst().asRGB()).isEqualTo(colorPair.getLast().asRGB());
        assertThat(colorPair.getFirst().asHSV()).isEqualTo(colorPair.getLast().asHSV());
    }
}
