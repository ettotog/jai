package io.gitlab.ettotog.jai.img;

import io.gitlab.ettotog.jai.img.color.RGB;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.vector.Vector;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class VectorImageTest {


    @Test
    public void writeAndRead(@TempDir Path tempDir) throws IOException {
        Image.IO io = IO.RGB_PNG;
        ImageFactory imageFactory = BufferedImageFactory.rgb();
        Vector<Double> randomImage = Vector.from(new Random().doubles(15).map(it -> Math.round(it * 255)).boxed());
        Dimension2D size = Dimension2D.of(3, 5);
        Image image = Image.from(size, randomImage, it -> RGB.of(it.intValue()));
        MutableImage mutableImage = imageFactory.create(size);
        image.entries().forEach(mutableImage::set);
        Path imageFile = Path.of(tempDir.toString(), "output.png");
        io.write(mutableImage, imageFile);
        Image readImage = io.read(imageFile);
        Vector<Double> readVector = readImage.asVector(color -> color.asRGB().getRed());
        assertThat(randomImage).isEqualTo(readVector);
    }
}
