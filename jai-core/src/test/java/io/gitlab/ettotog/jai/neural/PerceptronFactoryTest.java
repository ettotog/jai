package io.gitlab.ettotog.jai.neural;

import io.gitlab.ettotog.jai.math.functions.Sigmoid;
import lombok.val;
import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class PerceptronFactoryTest {

    @Test
    public void testCreate() {
        PerceptronFactory perceptronFactory = PerceptronFactory.builder()
                .activationFunction(new Sigmoid())
                .random(new Random(13L))
                .build();
        PerceptronImpl perceptron = perceptronFactory.create(5);
        assertThat(perceptron.getSize()).isEqualTo(5);
    }

    @Test
    public void createEquals() {
        val activationFunction = new Sigmoid();
        PerceptronFactory factoryA = PerceptronFactory.builder()
                .activationFunction(activationFunction)
                .random(new Random(13L))
                .build();
        PerceptronFactory factoryB = PerceptronFactory.builder()
                .activationFunction(activationFunction)
                .random(new Random(13L))
                .build();
        val a = factoryA.create(5);
        val b = factoryB.create(5);
        assertThat(a.getWeights()).isEqualTo(b.getWeights());
        assertThat(a.getActivationFunction()).isEqualTo(b.getActivationFunction());
        assertThat(a).isNotSameAs(b);
        Set<Perceptron> set = Set.of(a, b);
        assertThat(set).size().isEqualTo(2);
    }

}