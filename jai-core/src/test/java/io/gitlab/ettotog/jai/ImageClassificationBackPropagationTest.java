package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.img.BufferedImageReader;
import io.gitlab.ettotog.jai.img.Image;
import io.gitlab.ettotog.jai.img.color.RGB;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.DimensionalSize;
import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.array.Array;
import io.gitlab.ettotog.jai.math.array.Slicer;
import io.gitlab.ettotog.jai.math.functions.Sigmoid;
import io.gitlab.ettotog.jai.math.matrix.Matrix;
import io.gitlab.ettotog.jai.math.matrix.MatrixArrayAdapter;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.neural.BackPropagation;
import io.gitlab.ettotog.jai.neural.Network;
import io.gitlab.ettotog.jai.neural.layers.*;
import io.gitlab.ettotog.jai.sample.InMultipleFiles;
import io.gitlab.ettotog.jai.sample.LearningSample;
import io.gitlab.ettotog.jai.sample.VectorClassMapper;
import io.gitlab.ettotog.jai.swing.ImagePreviewer;
import lombok.SneakyThrows;
import lombok.val;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class ImageClassificationBackPropagationTest {

    static final long LIMIT = 100000L;
    static final double TOLERANCE = 0.01d;

    VectorClassMapper<Integer> mapper = VectorClassMapper.from(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

    @SneakyThrows
    public Vector<Double> loadImage(Path path) {
        Image.Reader reader = new BufferedImageReader();
        Stream<Double> stream =
                reader.read(path)
                        .entries()
                        .map(Matrix.Entry::getValue)
                        .map(color -> color.asRGB().avg() / 255.);
        return Vector.from(stream);
    }

    public Vector<Double> toClass(Path path) {
        String number = path.getFileName().toString().split("\\.")[0].split("-")[1];
        int clazz = Integer.parseInt(number);
        return Vector.indexed(10, Set.of(clazz), 1.0, 0.0);
    }

    public void log(BackPropagation.Event event) {
        if (event.getEpoch() % 100 != 0) return;
        System.out.println(event);
        event.getSource().summary(mapper, event.getNetwork());
    }

    @Test
    public void testClassifyImages() throws IOException, URISyntaxException {
        Dimension2D dimension = Dimension2D.of(3, 5);
        URL resource = getClass().getClassLoader().getResource("pixed-digits");
        Path path = Path.of(resource.toURI());
        LearningSample<Vector<Double>, Vector<Double>> learningSample = new InMultipleFiles<>(
                Arrays.stream(path.toFile().listFiles()).map(File::toPath).collect(Collectors.toList()),
                this::loadImage,
                this::toClass,
                dimension.size() + 1,
                10
        );
        BackPropagation backPropagation = BackPropagation.builder()
                .learningSample(learningSample)
                .learningRate(0.3)
                .momentum(0.1)
                .listener(this::log)
                .build();
        Random random = new Random(13L);
        Network network = Network.builder()
                .layer(Input.factory(dimension.size()))
                .layer(Dense.factory(16, new Sigmoid(), random))
                .layer(Dense.factory(10, new Sigmoid(), random))
                .build();
        val evaluation = backPropagation.trainUntil(network, TOLERANCE, LIMIT);
        backPropagation.summary(mapper, network);
        backPropagation.summary(mapper, network.asBoolean());
        assertThat(evaluation.getLoss()).isCloseTo(0, Offset.offset(TOLERANCE));
    }

    @Test
    public void testCNNClassifyImages() throws IOException, URISyntaxException {
        boolean headless = GraphicsEnvironment.isHeadless();
        Dimension2D dimension = Dimension2D.of(3, 5);
        URL resource = getClass().getClassLoader().getResource("pixed-digits");
        Path path = Path.of(resource.toURI());
        LearningSample<Vector<Double>, Vector<Double>> learningSample = new InMultipleFiles<>(
                Arrays.stream(path.toFile().listFiles()).map(File::toPath).collect(Collectors.toList()),
                this::loadImage,
                this::toClass,
                dimension.size(),
                10
        );
        BackPropagation backPropagation = BackPropagation.builder()
                .learningSample(learningSample)
                .learningRate(0.3)
                .momentum(0.1)
                .listener(this::log)
                .build();
        Random random = new Random(2852317391238727680L);
        Network network = Network.builder()
                .layer(Input.factory(DimensionalSize.of(5, 3)))
                .layer(Conv.factory(Dimension2D.of(3), 8, new Sigmoid(), random))
                .layer(Dense.factory(10, new Sigmoid(), random))
                .build();
        val evaluation = backPropagation.trainUntil(network, TOLERANCE, LIMIT);
        if (!headless) {
            ImagePreviewer imagePreviewer = ImagePreviewer.create();
            network.addListener(new Network.Listener() {
                @Override
                public void onProcess(Network.OnProcessEvent event) {
                    if (event.getLayerIndex() != 1) return;
                    Layer<?> layer = event.getLayer();
                    Vector<Double> inputs = event.getInputs();
                    Vector<Double> outputs = event.getOutputs();
                    DimensionalSize outputDimension = layer.getOutputDimension().getSize();
                    int lastSize = outputDimension.size(outputDimension.count() - 1);
                    Dimension2D imageSize = Dimension2D.of(outputDimension.reduced());
                    Image inputImage = Image.from(imageSize, inputs, RGB::from);
                    Array<Double> array = Array.of(outputDimension, outputs);
                    val images = IntStream.range(0, lastSize)
                            .boxed()
                            .map(index -> array.sliced(Slicer.last(index)))
                            .map(MatrixArrayAdapter::of)
                            .map(it -> Image.from(it, RGB::from))
                            .map(it -> it.scaled(10))
                            .collect(Collectors.toList());
                    imagePreviewer.preview(Stream.concat(Stream.of(inputImage.scaled(10)), images.stream()).collect(Collectors.toList()));
                }
            });
            backPropagation.summary(mapper, network);
            imagePreviewer.getParent().setVisible(true);
            JOptionPane.showConfirmDialog(imagePreviewer.getParent(), "wait");
        }
        backPropagation.summary(mapper, network.asBoolean());
        assertThat(evaluation.getLoss()).isCloseTo(0, Offset.offset(TOLERANCE));
    }
}
