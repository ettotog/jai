package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.math.functions.NullFunction;
import io.gitlab.ettotog.jai.neural.PerceptronImpl;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PerceptronTest {


    @Test
    public void testZero() {
        PerceptronImpl perceptron = new PerceptronImpl()
                .setWeights(Vector.of(0d, 0d, 0d))
                .setActivationFunction(new NullFunction());
        perceptron.setConstantInputs(Vector.of(1d, 2d, 3d));
        assertThat(perceptron.evaluate()).isZero();
    }
}
