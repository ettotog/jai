package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.DimensionalSize;
import io.gitlab.ettotog.jai.math.functions.Sigmoid;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.neural.Network;
import io.gitlab.ettotog.jai.neural.io.NetworkReader;
import io.gitlab.ettotog.jai.neural.io.NetworkWriter;
import io.gitlab.ettotog.jai.neural.layers.Conv;
import io.gitlab.ettotog.jai.neural.layers.Dense;
import io.gitlab.ettotog.jai.neural.layers.Input;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.nio.file.Path;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class SerializeTest {

    private static Network network;

    @BeforeAll
    public static void setupNetwork() {
        Random random = new Random(13L);
        network = Network.builder()
                .layer(Input.factory(DimensionalSize.of(5, 3)))
                .layer(Conv.factory(Dimension2D.of(3), 8, new Sigmoid(), random))
                .layer(Dense.factory(10, new Sigmoid(), random))
                .build();
    }


    @Test
    @SneakyThrows
    public void testDenseSerialize(@TempDir Path path) {
        File outputFile = path.resolve("layer.obj").toFile();
        Dense layer = (Dense) network.getLayers().get(2);
        Dense.Serialized serializable = layer.serialize();
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(outputFile))) {
            objectOutputStream.writeObject(serializable);
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(outputFile))) {
            Dense newLayer = new Dense.Deserializer().deserialize((Dense.Serialized) objectInputStream.readObject());
            assertThat(newLayer.serialize()).isEqualTo(serializable);
        }
    }

    @Test
    @SneakyThrows
    public void testInputSerialize(@TempDir Path path) {
        File outputFile = path.resolve("layer.obj").toFile();
        Input layer = (Input) network.getLayers().get(0);
        Input.Serialized serializable = layer.serialize();
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(outputFile))) {
            objectOutputStream.writeObject(serializable);
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(outputFile))) {
            Input newLayer = new Input.Deserializer().deserialize((Input.Serialized) objectInputStream.readObject());
            assertThat(newLayer.serialize()).isEqualTo(serializable);
        }
    }

    @Test
    @SneakyThrows
    public void testConvSerialize(@TempDir Path path) {
        File outputFile = path.resolve("layer.obj").toFile();
        Conv layer = (Conv) network.getLayers().get(1);
        Conv.Serialized serializable = layer.serialize();
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(outputFile))) {
            objectOutputStream.writeObject(serializable);
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(outputFile))) {
            Conv newLayer = new Conv.Deserializer().deserialize((Conv.Serialized) objectInputStream.readObject());
            assertThat(newLayer.serialize()).isEqualTo(serializable);
        }
    }

    @Test
    @SneakyThrows
    public void readWriteNetwork(@TempDir Path path) {
        File outputFile = path.resolve("network.obj").toFile();
        try (NetworkWriter networkWriter = new NetworkWriter(new FileOutputStream(outputFile))) {
            networkWriter.write(network);
        }
        NetworkReader reader = NetworkReader.create();
        try (FileInputStream inputStream = new FileInputStream(outputFile)) {
            Network newNetwork = reader.read(inputStream);
            Vector<Double> random = Vector.random(15);
            newNetwork.setInputs(random);
            network.setInputs(random);
            assertThat(newNetwork.getOutput()).isEqualTo(network.getOutput());
        }
    }

}
