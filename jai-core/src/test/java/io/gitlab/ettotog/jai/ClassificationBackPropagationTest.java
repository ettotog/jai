package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.math.functions.Sigmoid;
import io.gitlab.ettotog.jai.neural.*;
import io.gitlab.ettotog.jai.neural.layers.Dense;
import io.gitlab.ettotog.jai.neural.layers.Input;
import io.gitlab.ettotog.jai.sample.LearningSample;
import io.gitlab.ettotog.jai.sample.VectorClassMapper;
import lombok.val;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class ClassificationBackPropagationTest {

    static final long LIMIT = 100000L;
    static final double TOLERANCE = 0.01d;

    @Test
    public void testBinToDec() {
        VectorClassMapper<Integer> mapper = VectorClassMapper.from(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        BackPropagation backPropagation = BackPropagation.builder()
                .learningSample(LearningSample.binaryToDecimal())
                .learningRate(0.3)
                .momentum(0.1)
                .build();
        Random random = new Random(13L);
        Network network = Network.builder()
                .layer(Input.factory(4))
                .layer(Dense.factory(10, new Sigmoid(), random))
                .build();
        val evaluation = backPropagation.trainUntil(network, TOLERANCE, LIMIT);
        backPropagation.summary(mapper, network);
        backPropagation.summary(network.asBoolean());
        assertThat(evaluation.getLoss()).isCloseTo(0, Offset.offset(TOLERANCE));
    }
}
