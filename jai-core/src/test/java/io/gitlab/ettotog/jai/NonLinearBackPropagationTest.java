package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.sample.LearningSample;
import io.gitlab.ettotog.jai.math.functions.DerivableFunction;
import io.gitlab.ettotog.jai.math.functions.Sigmoid;
import io.gitlab.ettotog.jai.math.functions.TanH;
import io.gitlab.ettotog.jai.neural.BackPropagation;
import io.gitlab.ettotog.jai.neural.Network;
import io.gitlab.ettotog.jai.neural.PerceptronFactory;
import lombok.val;
import org.assertj.core.data.Offset;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class NonLinearBackPropagationTest {

    static final long LIMIT = 100000L;
    static final double TOLERANCE = 0.01d;

    public static Stream<DerivableFunction> provideActivationFunction() {
        return Stream.of(
                new Sigmoid(),
                new TanH()
        );
    }

    @ParameterizedTest
    @MethodSource("provideActivationFunction")
    public void testXor(DerivableFunction activationFunction) {
        PerceptronFactory factory = PerceptronFactory.builder()
                .random(new Random(13L))
                .activationFunction(activationFunction)
                .build();
        BackPropagation backPropagation = BackPropagation.builder()
                .learningSample(LearningSample.xorWithBias())
                .learningRate(0.3)
                .momentum(0.1)
                .build();
        Network network = Network.connectWithBias(
                factory.layer(3, 2),
                factory.layer(3, 1)
        );
        val evaluation = backPropagation.trainUntil(network, TOLERANCE, LIMIT);
        assertThat(evaluation.getLoss()).isCloseTo(0, Offset.offset(TOLERANCE));
    }
}
