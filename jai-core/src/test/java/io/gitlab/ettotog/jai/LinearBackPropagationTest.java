package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.sample.LearningSample;
import io.gitlab.ettotog.jai.math.functions.*;
import io.gitlab.ettotog.jai.neural.BackPropagation;
import io.gitlab.ettotog.jai.neural.Network;
import io.gitlab.ettotog.jai.neural.Perceptron;
import io.gitlab.ettotog.jai.neural.PerceptronFactory;
import lombok.val;
import org.assertj.core.data.Offset;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


public class LinearBackPropagationTest {

    static final long LIMIT = 10000L;
    static final double TOLERANCE = 0.01d;

    public static Stream<DerivableFunction> provideActivationFunction() {
        return Stream.of(
                new Sigmoid(),
                new Threshold(),
                new Linear(),
                new TanH(),
                new ReLU()
        );
    }

    @ParameterizedTest
    @MethodSource("provideActivationFunction")
    public void testNoop(DerivableFunction activationFunction) {
        PerceptronFactory factory = PerceptronFactory.builder()
                .random(new Random(1875647606144535552L))
                .activationFunction(activationFunction)
                .build();
        BackPropagation backPropagation = BackPropagation.builder()
                .learningSample(LearningSample.noopWithBias(1d, 0d))
                .build();
        Perceptron perceptron = factory.create(2);
        val evaluation = backPropagation.trainUntil(perceptron, TOLERANCE, LIMIT);
        assertThat(evaluation.getLoss()).isCloseTo(0, Offset.offset(TOLERANCE));
    }

    @ParameterizedTest
    @MethodSource("provideActivationFunction")
    public void testNot(DerivableFunction activationFunction) {
        PerceptronFactory factory = PerceptronFactory.builder()
                .random(new Random(1280260069874673664L))
                .activationFunction(activationFunction)
                .build();
        BackPropagation backPropagation = BackPropagation.builder()
                .learningSample(LearningSample.notWithBias())
                .build();
        Perceptron perceptron = factory.create(2);
        val evaluation = backPropagation.trainUntil(perceptron, TOLERANCE, LIMIT);
        assertThat(evaluation.getLoss()).isCloseTo(0, Offset.offset(TOLERANCE));
    }

    @ParameterizedTest
    @MethodSource("provideActivationFunction")
    public void testAnd(DerivableFunction activationFunction) {
        PerceptronFactory factory = PerceptronFactory.builder()
                .random(new Random(7701843664414828544L))
                .activationFunction(activationFunction)
                .build();
        BackPropagation backPropagation = BackPropagation.builder()
                .learningSample(LearningSample.andWithBias())
                .build();
        Perceptron perceptron = factory.create(3);
        backPropagation.trainUntil(perceptron, TOLERANCE, LIMIT);
        val evaluation = MeanSquaredError.evaluator().create(LearningSample.andWithBias())
                .evaluate(Network.of(perceptron).asBoolean());
        assertThat(evaluation.getLoss()).isCloseTo(0, Offset.offset(TOLERANCE));
    }

    @ParameterizedTest
    @MethodSource("provideActivationFunction")
    public void testOr(DerivableFunction activationFunction) {
        PerceptronFactory factory = PerceptronFactory.builder()
                .random(new Random(7996944697364718592L))
                .activationFunction(activationFunction)
                .build();
        BackPropagation backPropagation = BackPropagation.builder()
                .learningSample(LearningSample.orWithBias())
                .build();
        Perceptron perceptron = factory.create(3);
        backPropagation.trainUntil(perceptron, TOLERANCE, LIMIT);
        val evaluation = MeanSquaredError.evaluator().create(LearningSample.orWithBias())
                .evaluate(Network.of(perceptron).asBoolean());
        assertThat(evaluation.getLoss()).isCloseTo(0, Offset.offset(TOLERANCE));
    }

}
