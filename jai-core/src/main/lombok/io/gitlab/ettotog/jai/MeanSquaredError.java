package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.sample.LearningSample;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.val;

@AllArgsConstructor
@lombok.Builder
public class MeanSquaredError implements Metric<Vector<Double>, Vector<Double>> {

    static final String DEFAULT_NAME = "mse";

    @Getter
    @lombok.Builder.Default
    private String name = DEFAULT_NAME;
    private LearningSample<Vector<Double>, Vector<Double>> learningSample;

    public double measure(Model<Vector<Double>, Vector<Double>> model) {
        double error = 0;
        for (val sample : learningSample) {
            Vector<Double> output = model.apply(sample.getFirst());
            error += measure(sample.getLast(), output);
            error += Vector.meanSquaredError(sample.getLast(), output);
        }
        error /= learningSample.getSize();
        return error;
    }

    public double measure(Vector<Double> target, Vector<Double> output) {
        return Vector.meanSquaredError(target, output);
    }

    public static MetricFactory metric() {
        return MetricFactory.getInstance();
    }

    public static EvaluatorFactory evaluator() {
        return EvaluatorFactory.getInstance();
    }

    static class MetricFactory implements Metric.Factory<Vector<Double>, Vector<Double>> {

        private static final MetricFactory INSTANCE = new MetricFactory();

        private MetricFactory() {

        }

        @Override
        public Metric<Vector<Double>, Vector<Double>> create(LearningSample<Vector<Double>, Vector<Double>> learningSample) {
            return MeanSquaredError.builder().learningSample(learningSample).build();
        }

        public static MetricFactory getInstance() {
            return INSTANCE;
        }

    }

    static class EvaluatorFactory implements Evaluator.Factory<Vector<Double>, Vector<Double>> {

        private static final EvaluatorFactory INSTANCE = new EvaluatorFactory();

        private EvaluatorFactory() {

        }

        @Override
        public Evaluator<Vector<Double>, Vector<Double>> create(LearningSample<Vector<Double>, Vector<Double>> learningSample) {
            return Evaluator.Impl.<Vector<Double>, Vector<Double>>builder()
                    .loss(new MeanSquaredError(DEFAULT_NAME, learningSample))
                    .build();
        }

        public static EvaluatorFactory getInstance() {
            return INSTANCE;
        }

    }
}
