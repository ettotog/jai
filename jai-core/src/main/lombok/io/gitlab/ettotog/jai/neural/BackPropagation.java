package io.gitlab.ettotog.jai.neural;

import io.gitlab.ettotog.jai.*;
import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.sample.IntoClass;
import io.gitlab.ettotog.jai.sample.LearningSample;
import lombok.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.gitlab.ettotog.jai.math.vector.Vector.sum;
import static io.gitlab.ettotog.jai.math.vector.Vector.zip;
import static io.gitlab.ettotog.jai.math.vector.Vector.collector;

@lombok.Builder
public class BackPropagation {

    @AllArgsConstructor
    @Getter
    @EqualsAndHashCode
    public static class Event {

        private final long epoch;
        private final Evaluator.Evaluation evaluation;
        private final BackPropagation source;
        private final Network network;

        @Override
        public String toString() {
            return "Event{" +
                    "epoch=" + epoch +
                    ", evaluation=" + evaluation +
                    '}';
        }
    }

    @FunctionalInterface
    public interface Listener {

        void onIteration(Event event);
    }

    class Node {
        double delta;
        Vector<Double> gradient;
        Vector<Double> variation;
        Perceptron perceptron;

        public Node(Perceptron perceptron) {
            this.perceptron = perceptron;
            this.gradient = Vector.zeros(perceptron.getSize());
            this.variation = Vector.zeros(perceptron.getSize());
        }

        void calcDelta(double error) {
            double derived = this.perceptron.getActivationFunction()
                    .derivative()
                    .apply(perceptron.evaluate());
            this.delta = derived * error;
        }

        void calcGradient() {
            gradient = perceptron.getInputs()
                    .values()
                    .map(it -> it.evaluate() * delta)
                    .collect(collector());
        }

        void calcVariation() {
            variation = zip(gradient, variation)
                    .map(p -> p.getFirst() * learningRate + momentum * p.getLast())
                    .collect(collector());
        }

        void updateWidths() {
            val weights = sum(perceptron.getWeights(), variation);
            perceptron.setWeights(weights);
        }

        public Map<Perceptron, Double> apply(double error) {
            calcDelta(error);
            calcGradient();
            calcVariation();
            Map<Perceptron, Double> errors = zip(perceptron.getInputs(), perceptron.getWeights())
                    .filter(it -> it.getFirst() instanceof Perceptron)
                    .collect(Collectors.toMap(it -> (Perceptron) it.getFirst(), it -> it.getLast() * delta, Double::sum));
            updateWidths();
            return errors;
        }
    }

    private LearningSample<Vector<Double>, Vector<Double>> learningSample;
    @lombok.Builder.Default
    private double learningRate = 0.3;

    @lombok.Builder.Default
    private double momentum = 0.1;

    @lombok.Builder.Default

    private Map<Perceptron, Node> nodeMap = new HashMap<>();

    @lombok.Builder.Default
    private final Evaluator.Factory<Vector<Double>, Vector<Double>> evaluatorFactory = MeanSquaredError.evaluator();


    @Singular
    private List<Listener> listeners;

    public Map<Perceptron, Double> train(Perceptron perceptron, double error) {
        Node node = nodeMap.getOrDefault(perceptron, new Node(perceptron));
        val errors = node.apply(error);
        nodeMap.put(perceptron, node);
        return errors;
    }

    public Evaluator.Evaluation trainUntil(Network network, double maxError, long limit) {
        long iteration = 0;
        val evaluator = evaluatorFactory.create(learningSample);
        Evaluator.Evaluation evaluation = null;
        while (iteration < limit || limit == -1) {
            evaluation = evaluator.evaluate(network);
            Event event = new Event(
                    iteration,
                    evaluation,
                    this,
                    network
            );
            for (Listener listener : listeners) {
                listener.onIteration(event);
            }
            if (evaluation.getLoss() <= maxError) return evaluation;
            train(network);
            iteration += 1;
        }
        return evaluation;
    }

    public Evaluator.Evaluation trainUntil(Perceptron perceptron, double maxError, long limit) {
        Network network = Network.of(perceptron);
        return trainUntil(network, maxError, limit);
    }

    public Map<Perceptron, Double> mergeErrors(Map<Perceptron, Double> a, Map<Perceptron, Double> b) {
        Map<Perceptron, Double> result = new HashMap<>(a);
        for (val entry : b.entrySet()) {
            double error = result.getOrDefault(entry.getKey(), 0d) + entry.getValue();
            result.put(entry.getKey(), error);
        }
        return result;
    }

    public Map<Perceptron, Double> getErrors(Network network, Vector<Double> desiredOutput) {
        Map<Perceptron, Double> errors = new HashMap<>();
        val output = network.getOutput();
        for (int i = 0; i < output.size(); i += 1) {
            Evaluable<Double> doubleEvaluable = network.getLastLayer().get(i);
            if (doubleEvaluable instanceof Perceptron) {
                errors.put((Perceptron) doubleEvaluable, desiredOutput.get(i) - output.get(i));
            }
        }
        return errors;
    }

    public void train(Network network) {
        for (val sample : learningSample) {
            Vector<Double> input = sample.getFirst();
            Vector<Double> desiredOutput = sample.getLast();
            network.setInputs(input);
            network.process();
            Map<Perceptron, Double> outputErrors = getErrors(network, desiredOutput);
            while (!outputErrors.isEmpty()) {
                for (val entry : outputErrors.entrySet()) {
                    outputErrors = mergeErrors(outputErrors, train(entry.getKey(), entry.getValue()));
                    outputErrors.remove(entry.getKey());
                }
            }
        }
    }


    public void summary(Model<Vector<Double>, Vector<Double>> model) {
        double avgError = 0;
        for (int i = 0; i < learningSample.getSize(); i += 1) {
            model.setInputs(learningSample.getInput(i));
            model.process();
            val output = model.getOutput();
            double error = Vector.meanSquaredError(output, learningSample.getOutput(i));
            System.out.printf(
                    "%s | %s | %s | Error: %s \n",
                    learningSample.getInput(i), output, learningSample.getOutput(i),
                    error
            );
            avgError += error;
        }
        avgError /= learningSample.getOutputWidth();
        System.out.println(avgError);
        System.out.println();
    }

    public void summary(IntoClass<?, Vector<Double>> intoClass, Model<Vector<Double>, Vector<Double>> model) {
        val evaluator = evaluatorFactory.create(learningSample);
        for (val sample : learningSample) {
            model.setInputs(sample.getFirst());
            model.process();
            val output = model.getOutput();
            System.out.printf(
                    "%s | %s | %s \n",
                    sample.getFirst(),
                    intoClass.toClasses(output),
                    intoClass.toClasses(sample.getLast())
            );
        }
        val avgError = evaluator.evaluate(model);
        System.out.println(avgError);
        System.out.println();
    }
}
