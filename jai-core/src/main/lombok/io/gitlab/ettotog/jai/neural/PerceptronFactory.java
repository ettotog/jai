package io.gitlab.ettotog.jai.neural;

import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.math.functions.DerivableFunction;
import io.gitlab.ettotog.jai.neural.layers.Layer;
import io.gitlab.ettotog.jai.neural.layers.LayerImpl;
import lombok.AllArgsConstructor;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@AllArgsConstructor
@lombok.Builder
public class PerceptronFactory {

    @lombok.Builder.Default
    private final Random random = new Random();
    private final DerivableFunction activationFunction;

    public PerceptronImpl create(int size) {
        return new PerceptronImpl()
                .setActivationFunction(activationFunction)
                .setWeights(Vector.from(random.doubles().map(it -> 1 - it * 2).limit(size).boxed()));
    }

    public Layer layer(int inputs, int size) {
        return LayerImpl.of(IntStream.range(0, size)
                .mapToObj(it -> create(inputs))
                .collect(Collectors.toList()));
    }

}
