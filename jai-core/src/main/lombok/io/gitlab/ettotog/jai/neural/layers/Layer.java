package io.gitlab.ettotog.jai.neural.layers;

import io.gitlab.ettotog.jai.Model;
import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;

import java.io.Serializable;
import java.util.Iterator;
import java.util.stream.Stream;

public interface Layer<S extends Serializable> extends Iterable<Evaluable<Double>>, Model<Vector<Evaluable<Double>>, Vector<Double>> {

    LayerDimension getOutputDimension();

    Evaluable<Double> get(int index);

    default void process() {

    }

    Stream<? extends Evaluable<Double>> stream();

    @Override
    default Vector<Double> getOutput() {
        return Vector.from(stream().map(Evaluable::evaluate));
    }

    @Override
    default Iterator<Evaluable<Double>> iterator() {
        return (Iterator<Evaluable<Double>>) stream().iterator();
    }

    interface Factory {

        Layer<?> create(LayerDimension inputParameters);

    }

    interface Deserializer<S extends Serializable> {

        boolean accept(Object serializable);

        Layer<S> deserialize(S serializable);

        default Layer<? extends Serializable> deserialize(Object serializable) {
            if (!accept(serializable)) throw new RuntimeException();
            return deserialize((S) serializable);
        }

    }

    S serialize();
}

