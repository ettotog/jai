package io.gitlab.ettotog.jai.neural;

import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.math.functions.DerivableFunction;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

@AllArgsConstructor
public class FlyweightPerceptron extends AbstractPerceptron {

    private double[] sharedWeights;
    private List<Evaluable<Double>> sharedInputs;
    private DerivableFunction activationFunction;
    private int offset;
    private int length;

    @Override
    public int getSize() {
        return length;
    }

    @Override
    public Vector<Double> getWeights() {
        return Vector.from(sharedWeights, offset, length);
    }

    @Override
    public Perceptron setWeights(Vector<Double> weights) {
        IntStream.range(0, weights.size())
                .forEach(it -> sharedWeights[it + this.offset] = weights.get(it));
        return this;
    }

    @Override
    public Perceptron setInputs(Vector<Evaluable<Double>> inputs) {
        for (int i = 0; i < inputs.size(); i += 1) {
            sharedInputs.set(i, inputs.get(i));
        }
        return this;
    }

    @Override
    public Vector<Evaluable<Double>> getInputs() {
        return Vector.from(sharedInputs);
    }

    @Override
    public DerivableFunction getActivationFunction() {
        return activationFunction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlyweightPerceptron)) return false;
        FlyweightPerceptron that = (FlyweightPerceptron) o;
        return offset == that.offset && sharedWeights == that.sharedWeights;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(offset);
        result = 31 * result + Objects.hashCode(sharedWeights);
        return result;
    }

    @Override
    public String toString() {
        return "FlyweightPerceptron{" +
                "activationFunction=" + activationFunction +
                ", offset=" + offset +
                ", length=" + length +
                '}';
    }
}
