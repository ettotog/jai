package io.gitlab.ettotog.jai.neural.layers;

import com.google.auto.service.AutoService;
import io.gitlab.ettotog.jai.math.DimensionalSize;
import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.neural.io.AbstractLayerDeserializer;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@AllArgsConstructor
public class Input implements Layer<Input.Serialized> {

    private DimensionalSize size;
    private Vector<Evaluable<Double>> inputs;

    @AllArgsConstructor
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class Serialized implements Serializable {
        private final DimensionalSize size;
    }

    @Override
    public void setInputs(Vector<Evaluable<Double>> inputs) {
        if (size.size() != inputs.size()) throw new IllegalArgumentException();
        this.inputs = inputs;
    }

    @Override
    public LayerDimension getOutputDimension() {
        return LayerDimension.of(size);
    }

    @Override
    public Evaluable<Double> get(int index) {
        return () -> inputs.get(index).evaluate();
    }

    @Override
    public Stream<? extends Evaluable<Double>> stream() {
        return IntStream.range(0, size.size()).mapToObj(this::get);
    }

    @Override
    public Serialized serialize() {
        return new Serialized(size);
    }

    @AllArgsConstructor
    public static class Factory implements Layer.Factory {

        private DimensionalSize size;

        @Override
        public Input create(LayerDimension inputParameters) {
            return new Input(size, null);
        }

    }


    public static Input.Factory factory(DimensionalSize size) {
        return new Factory(size);
    }

    public static Input.Factory factory(int size) {
        return new Factory(DimensionalSize.of(size));
    }


    @AutoService(Layer.Deserializer.class)
    public static class Deserializer extends AbstractLayerDeserializer<Serialized> {

        public Deserializer() {
            super(Serialized.class);
        }

        @Override
        public Input deserialize(Serialized serializable) {
            return new Input(serializable.getSize(), null);
        }
    }

    @Override
    public String toString() {
        return "Input{" +
                "size=" + size +
                '}';
    }
}
