package io.gitlab.ettotog.jai.neural.layers;

import io.gitlab.ettotog.jai.math.DimensionalSize;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class LayerDimensionImpl implements LayerDimension {

    private final int bias;
    private final DimensionalSize dimensionalSize;


    public LayerDimensionImpl addBias() {
        return new LayerDimensionImpl(bias | 1, dimensionalSize);
    }

    @Override
    public int getLength() {
        return dimensionalSize.size() + bias;
    }

    @Override
    public DimensionalSize getSize() {
        return dimensionalSize;
    }

}
