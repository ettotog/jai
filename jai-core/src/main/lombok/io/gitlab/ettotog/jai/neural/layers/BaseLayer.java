package io.gitlab.ettotog.jai.neural.layers;

import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.neural.Perceptron;
import lombok.AllArgsConstructor;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

@AllArgsConstructor
public abstract class BaseLayer<S extends Serializable> implements Layer<S> {

    protected final double[] weights;
    protected final Vector<Perceptron> neurons;
    protected final LayerDimension outputDimension;

    @Override
    public void process() {
        neurons.values().forEach(Perceptron::process);
    }

    @Override
    public LayerDimension getOutputDimension() {
        return outputDimension;
    }

    @Override
    public Evaluable<Double> get(int index) {
        return neurons.get(index);
    }

    @Override
    public Stream<? extends Evaluable<Double>> stream() {
        return neurons.values();
    }

}
