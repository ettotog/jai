package io.gitlab.ettotog.jai.neural.layers;

import com.google.auto.service.AutoService;
import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.functions.DerivableFunction;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.neural.FlyweightPerceptron;
import io.gitlab.ettotog.jai.neural.Perceptron;
import io.gitlab.ettotog.jai.neural.io.AbstractLayerDeserializer;
import lombok.*;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Dense extends BaseLayer<Dense.Serialized> {

    public Dense(double[] weights, Vector<Perceptron> neurons, LayerDimension outputDimension) {
        super(weights, neurons, outputDimension);
    }

    @Override
    public void setInputs(Vector<Evaluable<Double>> input) {
        neurons.values().forEach(it -> it.setInputs(input));
    }

    static Vector<Perceptron> createNeurons(double[] weights, int length, DerivableFunction activationFunction) {
        int inputLength = weights.length / length;
        List<Evaluable<Double>> inputs = IntStream.range(0, inputLength).mapToObj(it -> Evaluable.zero()).collect(Collectors.toList());
        Vector<Perceptron> neurons = IntStream.range(0, length)
                .mapToObj(index -> new FlyweightPerceptron(
                        weights,
                        inputs,
                        activationFunction,
                        index * inputLength,
                        inputLength
                ))
                .collect(Vector.collector());
        return neurons;
    }

    @Override
    public Serialized serialize() {
        return new Serialized(
                Arrays.copyOf(weights, weights.length),
                outputDimension.getLength(),
                neurons.get(0).getActivationFunction()
        );
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class Serialized implements Serializable {
        private double[] weights;
        private int outputSize;
        private DerivableFunction activationFunction;
    }


    public static Dense.Factory factory(int size, DerivableFunction activationFunction, Random random) {
        return new Factory(size, activationFunction, random);
    }

    @AllArgsConstructor
    public static class Factory implements Layer.Factory {
        private final int length;

        private final DerivableFunction activationFunction;

        private final Random random;

        @Override
        public Dense create(LayerDimension parameter) {
            int inputLength = parameter.getLength();
            double[] weights = random.doubles().map(it -> 1 - it * 2).limit((long) inputLength * length).toArray();
            Vector<Perceptron> neurons = createNeurons(weights, length, activationFunction);
            return new Dense(
                    weights,
                    neurons,
                    LayerDimension.of(neurons.size())
            );
        }


    }


    @AutoService(Layer.Deserializer.class)
    public static class Deserializer extends AbstractLayerDeserializer<Serialized> {

        public Deserializer() {
            super(Serialized.class);
        }

        @Override
        public Dense deserialize(Serialized serializable) {
            return new Dense(
                    serializable.weights,
                    createNeurons(serializable.weights, serializable.outputSize, serializable.activationFunction),
                    LayerDimension.of(serializable.outputSize)
            );
        }

    }

}
