package io.gitlab.ettotog.jai.neural;

import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.math.functions.DerivableFunction;
import io.gitlab.ettotog.jai.math.functions.Sigmoid;
import lombok.Getter;
import lombok.Setter;
import lombok.val;

import static io.gitlab.ettotog.jai.math.vector.Vector.zip;
import static io.gitlab.ettotog.jai.math.vector.Vector.collector;

@Getter
@Setter
public class PerceptronImpl implements Evaluable<Double>, Perceptron {

    private Vector<Evaluable<Double>> inputs;

    private Vector<Double> weights;

    private DerivableFunction activationFunction = new Sigmoid();

    private Evaluable<Double> output = this::process;

    public Double process() {
        val result = zip(inputs, weights)
                .map(p -> p.getFirst().evaluate() * p.getLast())
                .reduce(Double::sum)
                .map(activationFunction)
                .orElseThrow();
        output = Evaluable.constant(result);
        return result;
    }

    @Override
    public Double evaluate() {
        return output.evaluate();
    }

    public PerceptronImpl setConstantInputs(Vector<Double> inputs) {
        this.inputs = inputs.values().map(Evaluable::constant).collect(collector());
        return this;
    }


    public int getSize() {
        return weights.size();
    }

    public PerceptronImpl setInputs(Vector<Evaluable<Double>> inputs) {
        this.inputs = inputs;
        return this;
    }

    public PerceptronImpl setWeights(Vector<Double> weights) {
        this.weights = weights;
        return this;
    }

    public PerceptronImpl setActivationFunction(DerivableFunction activationFunction) {
        this.activationFunction = activationFunction;
        return this;
    }

    @Override
    public String toString() {
        return "Perceptron{" +
                "weights=" + weights +
                ", activationFunction=" + activationFunction +
                '}';
    }
}
