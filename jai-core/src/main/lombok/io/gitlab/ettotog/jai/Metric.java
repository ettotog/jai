package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.sample.LearningSample;

public interface Metric<I, O> {

    double measure(Model<I, O> model);

    double measure(O target, O output);

    String getName();

    interface Factory<I, O> {
        Metric<I, O> create(LearningSample<I, O> learningSample);
    }
}
