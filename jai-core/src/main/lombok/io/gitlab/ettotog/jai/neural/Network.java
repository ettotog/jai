package io.gitlab.ettotog.jai.neural;

import io.gitlab.ettotog.jai.Model;
import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.neural.layers.Layer;
import io.gitlab.ettotog.jai.neural.layers.LayerImpl;
import io.gitlab.ettotog.jai.neural.layers.LayerDimension;
import lombok.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static io.gitlab.ettotog.jai.math.vector.Vector.collector;

@Data
@AllArgsConstructor
public class Network implements Model<Vector<Double>, Vector<Double>> {

    public Network(List<Layer<?>> layers) {
        this(layers, null, new LinkedList<>());
    }

    public interface Listener {
        void onProcess(OnProcessEvent event);
    }

    @AllArgsConstructor
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class OnProcessEvent {

        private final Network network;
        private final int layerIndex;
        private final Layer<?> layer;
        private final Vector<Double> inputs;
        private final Vector<Double> outputs;

    }

    private List<Layer<?>> layers;
    private Vector<Double> inputs;
    private List<Listener> listeners;

    public boolean addListener(Listener listener) {
        return listeners.add(listener);
    }

    public boolean removeListener(Listener listener) {
        return listeners.remove(listener);
    }

    public Layer<?> getFirstLayer() {
        return layers.get(0);
    }

    public Layer<?> getLastLayer() {
        return layers.get(layers.size() - 1);
    }

    public void setInputs(Vector<Double> inputs) {
        Vector<Evaluable<Double>> constantInputs = inputs.values().map(Evaluable::constant).collect(collector());
        getFirstLayer().setInputs(constantInputs);
    }

    public void process() {
        int size = layers.size();
        Vector<Double> previousInput = inputs;
        for (int i = 0; i < size; i += 1) {
            val layer = layers.get(i);
            layer.process();
            Vector<Double> output = layer.getOutput();
            OnProcessEvent event = new OnProcessEvent(this, i, layer, previousInput, output);
            for (Listener listener : listeners) {
                listener.onProcess(event);
            }
            previousInput = output;
        }
    }

    public Vector<Double> getOutput() {
        return getLastLayer().getOutput();
    }

    public static Network connectWithBias(Layer<?>... layers) {
        return connectWithBias(List.of(layers));
    }

    public static Network connectWithBias(List<Layer<?>> layers) {
        Layer<?> previousLayer = layers.get(0);
        for (int i = 1; i < layers.size(); i += 1) {
            val layer = layers.get(i);
            Vector<Evaluable<Double>> inputs = Vector.from(Stream.concat(
                    previousLayer.stream(),
                    Stream.of(Evaluable.constant(1d))
            ));
            layer.setInputs(inputs);
            previousLayer = layer;
        }
        return new Network(
                layers
        );
    }


    public static Builder builder() {
        return Builder.create();
    }

    public static Network of(Perceptron perceptron) {
        return new Network(List.of(LayerImpl.of(perceptron)));
    }

    public static class Builder {

        private final List<Layer.Factory> layersFactories = new LinkedList<>();

        private Builder() {
        }

        public static Builder create() {
            return new Builder();
        }

        public Builder layer(Layer.Factory... layerFactory) {
            layersFactories.addAll(List.of(layerFactory));
            return this;
        }

        public Network build() {
            LayerDimension connectionParameters = null;
            List<Layer<?>> layers = new ArrayList<>(layersFactories.size());
            for (Layer.Factory factory : layersFactories) {
                Layer<?> layer = factory.create(connectionParameters);
                layers.add(layer);
                connectionParameters = layer.getOutputDimension().addBias();
            }
            return connectWithBias(layers);
        }


    }
}
