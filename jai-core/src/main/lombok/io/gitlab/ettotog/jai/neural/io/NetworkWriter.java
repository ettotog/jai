package io.gitlab.ettotog.jai.neural.io;

import io.gitlab.ettotog.jai.neural.Network;
import io.gitlab.ettotog.jai.neural.layers.Layer;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

@AllArgsConstructor
public class NetworkWriter implements AutoCloseable {

    private OutputStream outputStream;

    @Override
    public void close() throws Exception {
        outputStream.close();
    }

    public void write(Network network) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        for (Layer layer : network.getLayers()) {
            objectOutputStream.writeObject(layer.serialize());
        }
    }
}
