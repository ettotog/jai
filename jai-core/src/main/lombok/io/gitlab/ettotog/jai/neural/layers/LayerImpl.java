package io.gitlab.ettotog.jai.neural.layers;

import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.neural.Perceptron;
import lombok.AllArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

@AllArgsConstructor
public class LayerImpl implements Layer<LayerImpl.Serialized> {

    public static class Serialized implements Serializable{

    }

    @Override
    public Serialized serialize() {
        return null;
    }

    private List<Perceptron> perceptronList;

    @Override
    public void forEach(Consumer<? super Evaluable<Double>> action) {
        perceptronList.forEach(action);
    }


    @Override
    public LayerDimension getOutputDimension() {
        return LayerDimension.of(perceptronList.size());
    }

    @Override
    public Perceptron get(int index) {
        return perceptronList.get(index);
    }

    @Override
    public Stream<Evaluable<Double>> stream() {
        return perceptronList.stream().map(it -> it);
    }


    public static LayerImpl of(Perceptron perceptron) {
        return new LayerImpl(List.of(perceptron));
    }

    public static LayerImpl of(List<Perceptron> perceptronList) {
        return new LayerImpl(perceptronList);
    }

    @Override
    public void setInputs(Vector<Evaluable<Double>> inputs) {
        for (Perceptron perceptron : perceptronList) {
            perceptron.setInputs(inputs);
        }
    }

    @Override
    public void process() {
        for (Perceptron perceptron : perceptronList) {
            perceptron.process();
        }
    }
}
