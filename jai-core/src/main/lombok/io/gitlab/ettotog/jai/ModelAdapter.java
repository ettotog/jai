package io.gitlab.ettotog.jai;

import lombok.AllArgsConstructor;


import java.util.function.Function;

@AllArgsConstructor
@lombok.Builder
public class ModelAdapter<I, O, In, Out> implements Model<I, O> {

    private final Model<In, Out> model;
    private final Function<I, In> prepareInput;
    private final Function<Out, O> prepareOutput;

    @Override
    public void process() {
        model.process();
    }

    @Override
    public void setInputs(I inputs) {
        model.setInputs(prepareInput.apply(inputs));
    }

    @Override
    public O getOutput() {
        return prepareOutput.apply(model.getOutput());
    }

    public <Input> ModelAdapter<Input, O, In, Out> adaptInput(Function<Input, I> inputAdapter) {
        return new ModelAdapter<>(model, inputAdapter.andThen(prepareInput), prepareOutput);
    }

    public <Output> ModelAdapter<I, Output, In, Out> adaptOutput(Function<? super O, ? extends Output> outputAdapter) {
        return new ModelAdapter<>(model, prepareInput, prepareOutput.andThen(outputAdapter));
    }

    static <Input, Output> ModelAdapter<Input, Output, Input, Output> from(Model<Input, Output> model) {
        return new ModelAdapter<>(model, it -> it, it -> it);
    }
}
