package io.gitlab.ettotog.jai.neural.layers;

import com.google.auto.service.AutoService;
import io.gitlab.ettotog.jai.math.Dimension2D;
import io.gitlab.ettotog.jai.math.DimensionalSize;
import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.Point2D;
import io.gitlab.ettotog.jai.math.functions.DerivableFunction;
import io.gitlab.ettotog.jai.math.matrix.Matrix;
import io.gitlab.ettotog.jai.math.matrix.SafeMatrix;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.neural.FlyweightPerceptron;
import io.gitlab.ettotog.jai.neural.Perceptron;
import io.gitlab.ettotog.jai.neural.io.AbstractLayerDeserializer;
import lombok.*;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Conv extends BaseLayer<Conv.Serialized> {

    private final Dimension2D kernel;
    private final Dimension2D input;
    private final DimensionalSize output;
    private final int bias;

    public Conv(Dimension2D kernel, int bias, double[] weights, DerivableFunction activationFunction, Dimension2D input, DimensionalSize output) {
        super(weights, createNeurons(output, kernel, bias, weights, activationFunction), LayerDimension.of(output));
        this.kernel = kernel;
        this.bias = bias;
        this.output = output;
        this.input = input;
    }

    @Override
    public void setInputs(Vector<Evaluable<Double>> inputs) {
        val safeMatrix = new SafeMatrix<>(Matrix.from(input, inputs));
        val kernelMatrix = Matrix.from(kernel, Vector.from(kernel.stream()));
        val xOffset = (kernel.getWidth() - 1) / 2;
        val yOffset = (kernel.getHeight() - 1) / 2;
        neurons.entries()
                .forEach(entry -> {
                    Vector<Integer> integerVector = output.asIndex(entry.getKey());
                    val offset = Point2D.of(-yOffset, -xOffset).translated(Point2D.of(integerVector.get(0), integerVector.get(1)));
                    List<Evaluable<Double>> perceptronInputs = Stream.concat(
                            kernelMatrix.entries()
                                    .map(Matrix.Entry::getValue)
                                    .map(offset::translate)
                                    .map(safeMatrix::get),
                            IntStream.range(0, bias).mapToObj(it -> Evaluable.one())
                    ).collect(Collectors.toList());
                    entry.getValue().setInputs(Vector.from(perceptronInputs));
                });
    }

    public static Vector<Perceptron> createNeurons(DimensionalSize output, Dimension2D kernel, int bias, double[] weights, DerivableFunction activationFunction) {
        return IntStream.range(0, output.size())
                .mapToObj(
                        (index) -> {
                            Vector<Integer> integerVector = output.asIndex(index);
                            return new FlyweightPerceptron(
                                    weights,
                                    IntStream.range(0, kernel.size() + bias).mapToDouble(it -> 0d).mapToObj(Evaluable::constant).collect(Collectors.toList()),
                                    activationFunction,
                                    integerVector.get(integerVector.size() - 1) * (kernel.size() + bias),
                                    kernel.size() + bias
                            );
                        })
                .collect(Vector.collector());
    }

    @Override
    public Serialized serialize() {
        return new Serialized(
                Arrays.copyOf(weights, weights.length),
                kernel,
                input,
                output,
                bias,
                neurons.get(0).getActivationFunction()
        );
    }

    @AllArgsConstructor
    @Builder(toBuilder = true)
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class Serialized implements Serializable {
        private double[] weights;
        private final Dimension2D kernel;
        private final Dimension2D input;
        private final DimensionalSize output;
        private final int bias;
        private final DerivableFunction activationFunction;
    }

    public static Conv.Factory factory(Dimension2D kernelSize, int outputs, DerivableFunction activationFunction, Random random) {
        return new Conv.Factory(kernelSize, outputs, activationFunction, random);
    }

    @AutoService(Layer.Deserializer.class)
    public static class Deserializer extends AbstractLayerDeserializer<Serialized> {

        public Deserializer() {
            super(Serialized.class);
        }

        @Override
        public Conv deserialize(Serialized serializable) {
            return new Conv(
                    serializable.getKernel(),
                    serializable.getBias(),
                    serializable.getWeights(),
                    serializable.getActivationFunction(),
                    serializable.getInput(),
                    serializable.getOutput()
            );
        }
    }

    @AllArgsConstructor
    public static class Factory implements Layer.Factory {
        private final Dimension2D kernel;
        private final int outputs;
        private final DerivableFunction activationFunction;
        private final Random random;

        @Override
        public Conv create(LayerDimension inputParameters) {
            val output = inputParameters.getSize().extended(outputs);
            val input = Dimension2D.of(inputParameters.getSize());
            int bias = inputParameters.getLength() - inputParameters.getSize().size();
            int weightsLength = (kernel.size() + bias) * outputs;
            double[] weights = random.doubles().map(it -> 1 - it * 2).limit(weightsLength).toArray();
            return new Conv(
                    kernel,
                    bias,
                    weights,
                    activationFunction,
                    input,
                    output
            );

        }

    }
}
