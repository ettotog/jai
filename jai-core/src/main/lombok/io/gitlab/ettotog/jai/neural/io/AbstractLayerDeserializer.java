package io.gitlab.ettotog.jai.neural.io;

import io.gitlab.ettotog.jai.neural.layers.Layer;
import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public abstract class AbstractLayerDeserializer<E extends Serializable> implements Layer.Deserializer<E> {

    private final Class<E> type;

    @Override
    public boolean accept(Object serializable) {
        return type.isInstance(serializable);
    }

    public Layer<? extends E> deserialize(Object serializable) {
        if (!accept(serializable)) throw new RuntimeException();
        return deserialize(type.cast(serializable));
    }
}
