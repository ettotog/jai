package io.gitlab.ettotog.jai.neural.layers;

import io.gitlab.ettotog.jai.math.DimensionalSize;

public interface LayerDimension {

    int getLength();

    DimensionalSize getSize();

    LayerDimension addBias();

    static LayerDimension of(int size) {
        return new LayerDimensionImpl(0, DimensionalSize.of(size));
    }

    static LayerDimension of(DimensionalSize size) {
        return new LayerDimensionImpl(0, size);
    }

}
