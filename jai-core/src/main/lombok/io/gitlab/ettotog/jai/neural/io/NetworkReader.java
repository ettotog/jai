package io.gitlab.ettotog.jai.neural.io;

import io.gitlab.ettotog.jai.neural.Network;
import io.gitlab.ettotog.jai.neural.layers.Layer;
import lombok.AllArgsConstructor;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

@AllArgsConstructor
public class NetworkReader {

    private List<Layer.Deserializer> deserializers;

    public Network read(InputStream inputStream) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        List<Layer<?>> layers = new LinkedList<>();
        try {
            while (true) {
                layers.add(deserialize(objectInputStream.readObject()));
            }
        } catch (EOFException ex) {

        }
        return Network.connectWithBias(layers);
    }

    public Layer<?> deserialize(Object object) throws IOException {
        for (Layer.Deserializer deserializer : deserializers) {
            if (deserializer.accept(object)) {
                return deserializer.deserialize(object);
            }
        }
        throw new IOException(String.format("Deserializer for %s was not found", object));
    }

    @SuppressWarnings("rawtypes")
    public static NetworkReader create() {
        ServiceLoader<Layer.Deserializer> load = ServiceLoader.load(Layer.Deserializer.class);
        List<Layer.Deserializer> deserializers = load.stream().map(ServiceLoader.Provider::get).collect(Collectors.toList());
        return new NetworkReader(deserializers);
    }

}
