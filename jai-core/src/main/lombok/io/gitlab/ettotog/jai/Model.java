package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.math.vector.Vector;

import java.util.function.Function;

import static io.gitlab.ettotog.jai.math.vector.Vector.collector;

public interface Model<I, O> extends Function<I, O> {

    void process();

    void setInputs(I inputs);

    O getOutput();

    @Override
    default O apply(I doubleVector) {
        setInputs(doubleVector);
        process();
        return getOutput();
    }

    default Model<Vector<Double>, Vector<Double>> asBoolean() {
        Function<Vector<Double>, Vector<Double>> asBool =
                vec -> vec.values().map(it -> it >= 0.5 ? 1d : 0d).collect(collector());
        return new ModelAdapter<>((Model<Vector<Double>, Vector<Double>>) this, asBool, asBool);

    }
}
