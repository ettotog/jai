package io.gitlab.ettotog.jai.neural;

import io.gitlab.ettotog.jai.math.Evaluable;

public abstract class AbstractPerceptron implements Perceptron {

    private Evaluable<Double> output = this::process;

    @Override
    public Double process() {
        Double value = Perceptron.super.process();
        output = Evaluable.constant(value);
        return value;
    }

    @Override
    public Double evaluate() {
        return output.evaluate();
    }
}
