package io.gitlab.ettotog.jai;

import io.gitlab.ettotog.jai.sample.LearningSample;
import lombok.*;

import java.util.List;
import java.util.Map;

@FunctionalInterface
public interface Evaluator<I, O> {
    Evaluation evaluate(Model<I, O> model);

    interface Factory<I, O> {
        Evaluator<I, O> create(LearningSample<I, O> learningSample);
    }

    @AllArgsConstructor
    @Getter
    @ToString
    @lombok.Builder
    class Evaluation {
        private final double loss;
        @Singular
        private Map<String, Double> metrics;
    }

    @AllArgsConstructor
    @lombok.Builder
    class Impl<I, O> implements Evaluator<I, O> {


        private final Metric<I, O> loss;

        @Singular
        private final List<Metric<I, O>> metrics;

        @Override
        public Evaluation evaluate(Model<I, O> model) {
            val loss = this.loss.measure(model);
            val builder = Evaluation.builder()
                    .loss(loss);
            builder.metric("loss", loss);
            for (val metric : metrics) {
                builder.metric(metric.getName(), metric.measure(model));
            }
            return builder.build();
        }
    }
}
