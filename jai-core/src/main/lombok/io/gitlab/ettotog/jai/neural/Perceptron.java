package io.gitlab.ettotog.jai.neural;

import io.gitlab.ettotog.jai.math.Evaluable;
import io.gitlab.ettotog.jai.math.vector.Vector;
import io.gitlab.ettotog.jai.math.functions.DerivableFunction;

import static io.gitlab.ettotog.jai.math.vector.Vector.zip;

public interface Perceptron extends Evaluable<Double> {

    int getSize();


    Vector<Double> getWeights();

    Perceptron setWeights(Vector<Double> weights);

    Perceptron setInputs(Vector<Evaluable<Double>> inputs);

    Vector<Evaluable<Double>> getInputs();

    default Double process() {
        return zip(getInputs(), getWeights())
                .map(p -> p.getFirst().evaluate() * p.getLast())
                .reduce(Double::sum)
                .map(getActivationFunction())
                .orElseThrow();
    }

    @Override
    default Double evaluate() {
        return process();
    }

    DerivableFunction getActivationFunction();


}
