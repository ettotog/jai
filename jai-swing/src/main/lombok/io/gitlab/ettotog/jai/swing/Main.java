package io.gitlab.ettotog.jai.swing;

import io.gitlab.ettotog.jai.img.Image;
import io.gitlab.ettotog.jai.img.color.RGB;
import io.gitlab.ettotog.jai.math.Dimension2D;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String... args) {
        JFrame jFrame = new JFrame();
        jFrame.setLayout(new BorderLayout());
        Image image = Image.from(
                Dimension2D.of(256, 256),
                point2D -> RGB.of((int)(point2D.getX() + point2D.getY()) / 2)
        );
        JImage jImage = new JImage(image);
        jFrame.add(jImage, BorderLayout.CENTER);
        jFrame.setLocationRelativeTo(null);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.pack();
        jFrame.setVisible(true);
        System.out.println("Hello World");
    }
}
