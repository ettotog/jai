package io.gitlab.ettotog.jai.swing;

import io.gitlab.ettotog.jai.img.Image;
import io.gitlab.ettotog.jai.img.color.RGB;
import io.gitlab.ettotog.jai.math.Dimension2D;

import javax.swing.*;
import java.awt.*;

public class JImage extends JPanel {

    private Image image;

    public JImage(Image image) {
        setImage(image);
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
        Dimension dimension = new Dimension(image.getSize().getWidth(), image.getSize().getHeight());
        this.setPreferredSize(dimension);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Dimension size = getSize();
        Image scaledImage = image.scaled(Dimension2D.of(size.width, size.height));
        scaledImage.entries().forEach(
                entry -> {
                    RGB rgb = entry.getValue().asRGB();
                    g.setColor(new Color((int) Math.round(rgb.getRed()), (int) Math.round(rgb.getGreen()), (int) Math.round(rgb.getBlue())));
                    g.drawRect(entry.getX(), entry.getY(), 1, 1);
                }
        );
    }
}
