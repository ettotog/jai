package io.gitlab.ettotog.jai.swing;

import io.gitlab.ettotog.jai.img.Image;
import lombok.AllArgsConstructor;

import javax.swing.*;
import java.awt.*;
import java.util.List;


@AllArgsConstructor
public class ImagePreviewer {

    private JFrame parent;

    public JFrame getParent() {
        return parent;
    }

    public static ImagePreviewer create() {
        JFrame jFrame = new JFrame();
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setLayout(new BoxLayout(jFrame.getContentPane(), BoxLayout.Y_AXIS));
        return new ImagePreviewer(jFrame);
    }

    public JPanel preview(List<Image> images) {
        JPanel panel = new JPanel(new FlowLayout());
        for (Image image : images) {
            panel.add(new JImage(image));
        }
        parent.add(panel);
        parent.pack();
        return panel;
    }
}
